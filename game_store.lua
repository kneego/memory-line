-----------------------------------------------------------------------------------------
--
-- store.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

local iap = require( 'knee.iap' )

local table = require( 'table' )
local native = require( 'native' )
local system = require( 'system' )

local sceneGroup
local wait_group
local current_skip_options
local shop_label

-- zoznam skip ID-ciek
local skip_options_count = R.arrays('skip_options_count')
local skip_options_id = R.arrays('skip_options_id')

function purchase_ok( transaction )
	utils.debug('game_store:purchase_ok')
	
	-- hide wait group
	wait_group.alpha = 0

	-- ak je to daky haluzny produkt, tak nic nerobime
	if skip_options_count[transaction.productIdentifier] == nil then end

	-- pripocitame skip kredit
	settings.skip_options = settings.skip_options + skip_options_count[transaction.productIdentifier]
	settings.store_used = true

	utils.saveSettings( settings )

	current_skip_options.text = R.strings('number_of_skip') .. settings.skip_options

	utils.debug('game_store:purchase_callback', skip_options_count[transaction.productIdentifier])
end

function purchase_cancelled( transaction )
	utils.debug('game_store:purchase_cancelled')
	
	-- hide wait group
	wait_group.alpha = 0

	native.showAlert( 'Notice', 'Purchase was cancelled.', { 'OK' } )
end

function purchase_failed( transaction )
	utils.debug('game_store:purchase_failed')
	
	-- hide wait group
	wait_group.alpha = 0

	native.showAlert( 'Notice', 'Purchase failed.', { 'OK' } )
end

function scene:create( event )
	-- init iap module
	iap.init()
	iap.purchase_ok = purchase_ok
	iap.purchase_failed = purchase_failed
	iap.purchase_cancelled = purchase_cancelled

	-- get price for products
	-- iap.load_products()

	-- init UI
	sceneGroup = self.view

	local bg = display.newImageRect( 'images/bg/1.jpg', screenW, screenH )
	bg.x = halfW
	bg.y = halfH
	sceneGroup:insert( bg )

	shop_label = display.newText( {
		text = R.strings('shop_label'),
		fontSize = 34,
		font = 'SimplyRounded',
		x = halfW,
		y = 50
	} )
	sceneGroup:insert( shop_label )

	local button_offset = halfH

	-- buy 1x skip
	local skip_1_button_group = display.newGroup( )
	sceneGroup:insert( skip_1_button_group )

	local skip_1_button = display.newImageRect( 'images/level-current.png', 81, 81 )
	skip_1_button.x = halfW - 130
	skip_1_button.y = button_offset
	skip_1_button_group:insert( skip_1_button )

	utils.handlerAdd(skip_1_button_group, 'touch', function ( self, event )
		if event.phase == 'began' then
			button_down( skip_1_button_group )
		elseif event.phase == 'ended' and skip_1_button_group.down then
			button_up( skip_1_button_group )

			wait_group.alpha = 1
			
			iap.purchase( 'memoryline1skip' )
		end

		return true
	end )

	local skip_1_label_1 = display.newText( {
		text = R.strings('buy_label_1'),
		fontSize = 12,
		font = 'SimplyRounded',
		x = halfW - 130,
		y = button_offset - 20
	} )
	skip_1_label_1:setFillColor( 1 )
	skip_1_button_group:insert( skip_1_label_1 )

	local skip_1_number = display.newText( {
		text = '1x',
		fontSize = 16,
		font = 'SimplyRounded',
		x = halfW - 130,
		y = button_offset - 2
	} )
	skip_1_number:setFillColor( 1 )
	skip_1_button_group:insert( skip_1_number )

	local skip_1_label_2 = display.newText( {
		text = R.strings('buy_label_2'),
		fontSize = 12,
		font = 'SimplyRounded',
		x = halfW - 130,
		y = button_offset + 18
	} )
	skip_1_label_2:setFillColor( 1 )
	skip_1_button_group:insert( skip_1_label_2 )

	-- buy 5x skip
	local skip_5_button_group = display.newGroup( )
	sceneGroup:insert( skip_5_button_group )

	local skip_5_button = display.newImageRect( 'images/level-current.png', 81, 81 )
	skip_5_button.x = halfW
	skip_5_button.y = button_offset
	skip_5_button_group:insert( skip_5_button )

	utils.handlerAdd(skip_5_button_group, 'touch', function ( self, event )
		if event.phase == 'began' then
			button_down( skip_5_button_group )
		elseif event.phase == 'ended' and skip_5_button_group.down then
			button_up( skip_5_button_group )

			wait_group.alpha = 1

			iap.purchase( 'memoryline5skip' )

			return true
		end
	end )

	local skip_5_label_1 = display.newText( {
		text = R.strings('buy_label_1'),
		fontSize = 12,
		font = 'SimplyRounded',
		x = halfW,
		y = button_offset - 20
	} )
	skip_5_label_1:setFillColor( 1 )
	skip_5_button_group:insert( skip_5_label_1 )

	local skip_5_number = display.newText( {
		text = '5x',
		fontSize = 16,
		font = 'SimplyRounded',
		x = halfW,
		y = button_offset - 2
	} )
	skip_5_number:setFillColor( 1 )
	skip_5_button_group:insert( skip_5_number )

	local skip_5_label_2 = display.newText( {
		text = R.strings('buy_label_2'),
		fontSize = 12,
		font = 'SimplyRounded',
		x = halfW,
		y = button_offset + 18
	} )
	skip_5_label_2:setFillColor( 1 )
	skip_5_button_group:insert( skip_5_label_2 )

	-- buy 10x skip
	local skip_10_button_group = display.newGroup( )
	sceneGroup:insert( skip_10_button_group )

	local skip_10_button = display.newImageRect( 'images/level-current.png', 81, 81 )
	skip_10_button.x = halfW + 130
	skip_10_button.y = button_offset
	skip_10_button_group:insert( skip_10_button )

	utils.handlerAdd(skip_10_button_group, 'touch', function ( self, event )
		if event.phase == 'began' then
			button_down( skip_10_button_group )
		elseif event.phase == 'ended' and skip_10_button_group.down then
			button_up( skip_10_button_group )
			
			wait_group.alpha = 1

			iap.purchase( 'memoryline10skip' )

			return true
		end
	end )

	local skip_10_label_1 = display.newText( {
		text = R.strings('buy_label_1'),
		fontSize = 12,
		font = 'SimplyRounded',
		x = halfW + 130,
		y = button_offset - 20
	} )
	skip_10_label_1:setFillColor( 1 )
	skip_10_button_group:insert( skip_10_label_1 )

	local skip_10_number = display.newText( {
		text = '10x',
		fontSize = 16,
		font = 'SimplyRounded',
		x = halfW + 130,
		y = button_offset - 2
	} )
	skip_10_number:setFillColor( 1 )
	skip_10_button_group:insert( skip_10_number )

	local skip_10_label_2 = display.newText( {
		text = R.strings('buy_label_2'),
		fontSize = 12,
		font = 'SimplyRounded',
		x = halfW + 130,
		y = button_offset + 18
	} )
	skip_10_label_2:setFillColor( 1 )
	skip_10_button_group:insert( skip_10_label_2 )

	-- current skip options
	current_skip_options = display.newText( {
		text = '',
		fontSize = 20,
		font = 'SimplyRounded',
		x = halfW,
		y = halfH + 80,
		width = screenW,
		align = 'center'
	} )
	sceneGroup:insert( current_skip_options )

	-- back button 
	local back_button_group = display.newGroup( )
	sceneGroup:insert( back_button_group )

	local back_icon = display.newImageRect( back_button_group, 'images/back.png', 16, 24 )
	back_icon.anchorX = 0
	back_icon.anchorY = 1
	back_icon.x = 10
	back_icon.y = screenH - 10

	back_label = display.newText( {
		text = 'Menu',
		fontSize = 20,
		font = 'SimplyRounded',
		x = 36,
		y = screenH - 10
	} )
	back_label.anchorX = 0
	back_label.anchorY = 1
	back_button_group:insert( back_label )

	back_button = display.newRect( 10, screenH - 10, back_label.width + 36, 24 )
	back_button.anchorX = 0
	back_button.anchorY = 1
	back_button.alpha = 0
	back_button.isHitTestable = true
	back_button_group:insert( back_button )

	utils.handlerAdd(back_button_group, 'touch', function ( self, event )
		if event.phase == 'began' then
			button_down( back_button_group )
		elseif event.phase == 'ended' and back_button_group.down then
			button_up( back_button_group )

			goto_scene( 'menu', 250 )

			return true
		end
	end )

	-- wait group
	wait_group = display.newGroup( )
	wait_group.alpha = 0
	sceneGroup:insert( wait_group )

	local wait_background = display.newRect( 0, 0, screenW, screenH )
	wait_background.anchorX = 0
	wait_background.anchorY = 0
	wait_background.alpha = 0.85
	wait_background:setFillColor( 0, 0, 0 )
	wait_background:setStrokeColor( 0, 0, 0 )
	wait_group:insert(wait_background)

	utils.handlerAdd(wait_background, 'touch', function()
		return true
	end)

	local wait_label = display.newText( {
		text = R.strings('please_wait'),
		fontSize = 20,
		font = 'SimplyRounded',
		x = halfW,
		y = halfH
	} )
	wait_group:insert( wait_label )
end

function scene:show( event )
	sceneGroup = self.view

	local phase = event.phase
	
	if phase == "will" then
		current_skip_options.text = R.strings('number_of_skip') .. settings.skip_options

	elseif phase == "did" then
		-- Called when the scene is now on screen
		-- 
		-- INSERT code here to make the scene come alive
		get_label( shop_label )

		ga:view('store')
		infinario:track('screen', {name = 'store'})
	end	
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then
		
		
	elseif phase == "did" then
		-- Called when the scene is now off screen

	end	
end

function scene:destroy( event )
	local sceneGroup = self.view
	
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene