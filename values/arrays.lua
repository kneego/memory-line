local _M = {
	default = {},
	sk = {},
	cs = {},
}

-- definicia konstant
local END = 1000
local LINE = 1001
local CURVE = 1002

_M.default.settings = {
	line_width = 3,
	line_color = {utils.color(0, 0, 0)},

	preview_line_width = 3,
	preview_line_color = {utils.color(193, 39, 45)},
}

_M.default.levels = {
	{
		name = '1 vlnka',
		tolerance = 30,
		shape = {
			{CURVE, 125, 150},
			{CURVE, 250, 100},
			{CURVE, 300, 200},
			{CURVE, 450, 150},
			{END, END, END}
		}
	},
	{
		name = '2 Jednotka',
		tolerance = 20,
		shape = {
			{LINE, 175, 150},
			{LINE, 250, 80},
			{LINE, 250, 200},
			{CURVE, 250, 240},
			{END, END, END}
		}
	},
	{
		name = '3 uzlik',
		tolerance = 35,
		shape = {
			{CURVE, 140, 170},
			{CURVE, 320, 120},
			{CURVE, 270, 100},
			{CURVE, 170, 120},
			{CURVE, 370, 170},
			{END, END, END}
		}
	},
	{
		name = '4 obdlznik',
		tolerance = 20,
		shape = {
			{CURVE, 140, 240},
			{LINE, 140, 100},
			{LINE, 350, 100},
			{LINE, 350, 200},
			{CURVE, 120, 200},
			{END, END, END}
		}
	},
	{
		name = '5 DVOJKA',
		tolerance = 15,
		shape = {
			{CURVE, 245, 150},
			{CURVE, 265, 110},
			{CURVE, 285, 100},
			{CURVE, 300, 109},
			{CURVE, 300, 150},
			{CURVE, 245, 240},
			{LINE, 320, 240},
			{END, END, END}
		}
	},
	{
		name = '6 hory',
		tolerance = 29,
		shape = {
			{LINE, 125, 250},
			{LINE, 160, 165},
			{LINE, 200, 250},
			{LINE, 240, 100},
			{LINE, 280, 250},
			{LINE, 320, 160},
			{LINE, 360, 250},
			{END, END, END}
		}
	},
	{
		name = '7 pismeno J',
		tolerance = 25,
		shape = {
			{CURVE, 175, 150},
			{CURVE, 130, 80},
			{CURVE, 350, 220},
			{CURVE, 250, 240},
			{END, END, END}
		}
	},
	{
		name = '8 slucka',
		tolerance = 35,
		shape = {
			{CURVE, 175, 150},
			{CURVE, 300, 80},
			{LINE, 100, 100},
			{CURVE, 300, 140},
			{CURVE, 200, 240},
			{END, END, END}
		}
	},
	{
		name = '9 pismeno G',
		tolerance = 35,
		shape = {
			{CURVE, 405, 50},
			{CURVE, 255, 40},
			{CURVE, 180, 180},
			{CURVE, 280, 240},
			{CURVE, 430, 240},
			{CURVE, 430, 150},
			{LINE, 330, 150},
			{END, END, END}
		}
	},
	{
		name = '10 labut',
		tolerance = 37,
		shape = {
			{CURVE, 125, 150},
			{CURVE, 250, 100},
			{CURVE, 250, 130},
			{CURVE, 100, 180},
			{CURVE, 300, 200},
			{CURVE, 450, 150},
			{END, END, END}
		}
	},
	{
		name = '11 trojuholnik trochu',
		tolerance = 25,
		shape = {
			{CURVE, 195, 150},
			{LINE, 400, 230},
			{CURVE, 250, 80},
			{LINE, 150, 180},
			{CURVE, 250, 150},
			{END, END, END}
		}
	},
	{
		name = '12',
		tolerance = 37,
		shape = {
			{CURVE, 425, 150},
			{CURVE, 400, 230},
			{CURVE, 250, 80},
			{CURVE, 360, 120},
			{LINE, 70, 130},
			{LINE, 150, 180},
			{END, END, END}
		}
	},
	{
		name = '13 down',
		tolerance = 30,
		shape = {
			{CURVE, 125, 70},
			{LINE, 400, 100},
			{LINE, 130, 130},
			{CURVE, 360, 160},
			{LINE, 140, 190},
			{CURVE, 320, 240},
			{END, END, END}
		}
	},
	{
		name = '14 srdce',
		tolerance = 30,
		shape = {
			{LINE, 250, 260},
			{CURVE, 150, 150},
			{CURVE, 190, 100},
			{CURVE, 250, 140},
			{CURVE, 310, 100},
			{CURVE, 350, 150},
			{CURVE, 250, 250},
			{END, END, END}
		}
	},
	{
		name = '15 haluz uzol',
		tolerance = 28,
		shape = {
			{CURVE, 90, 230},
			{CURVE, 390, 170},
			{CURVE, 190, 140},
			{CURVE, 180, 199},
			{LINE, 230, 199},
			{LINE, 230, 280},
			{END, END, END}
		}
	},
	{
		name = '16 ryba',
		tolerance = 29,
		shape = {
			{LINE, 130, 120},
			{CURVE, 130, 200},
			{LINE, 180, 150},
			{CURVE, 280, 100},
			{CURVE, 380, 150},
			{CURVE, 280, 200},
			{CURVE, 180, 150},
			{LINE, 130, 100},
			{END, END, END}
		}
	},
	{
		name = '17 hacik',
		tolerance = 25,
		shape = {
			{CURVE, 90, 150},
			{LINE, 170, 170},
			{CURVE, 120, 120},
			{CURVE, 150, 200},
			{CURVE, 200, 240},
			{CURVE, 280, 180},
			{CURVE, 300, 90},
			{LINE, 150, 80},
			{END, END, END}
		}
	},
	{
		name = '18 hacik',
		tolerance = 35,
		shape = {
			{LINE, 280, 140},
			{CURVE, 380, 140},
			{CURVE, 380, 220},
			{CURVE, 280, 170},
			{LINE, 190, 120},
			{LINE, 80, 200},
			{LINE, 183, 200},
			{LINE, 183, 70},
			{END, END, END}
		}
	},
	{
		name = '19 vtak',
		tolerance = 23,
		shape = {
			{LINE, 420, 150},
			{LINE, 390, 90},
			{CURVE, 290, 120},
			{LINE, 250, 200},
			{LINE, 220, 150},
			{LINE, 100, 100},
			{CURVE, 113, 140},
			{END, END, END}
		}
	},
	{
		name = '20 down',
		tolerance = 38,
		shape = {
			{LINE, 180, 50},
			{CURVE, 250, 100},
			{CURVE, 210, 170},
			{LINE, 90, 120},
			{CURVE, 160, 230},
			{CURVE, 450, 140},
			{END, END, END}
		}
	},
	{
		name = '21 domcek',
		tolerance = 30,
		shape = {
			{CURVE, 180, 180},
			{LINE, 240, 30},
			{LINE, 300, 150},
			{LINE, 190, 150},
			{LINE, 190, 300},
			{LINE, 300, 300},
			{CURVE, 300, 80},
			{END, END, END}
		}
	},
	{
		name = '22 haluz',
		tolerance = 45,
		shape = {
			{CURVE, 332, 300},
			{CURVE, 160, 150},
			{CURVE, 300, 40},
			{CURVE, 300, 130},
			{CURVE, 420, 250},
			{LINE, 460, 80},
			{END, END, END}
		}
	},
	{
		name = '23 sipka',
		tolerance = 30,
		shape = {
			{CURVE, 200, 200},
			{CURVE, 310, 270},
			{CURVE, 420, 159},
			{LINE, 300, 60},
			{LINE, 320, 200},
			{LINE, 460, 60},
			{CURVE, 320, 70},
			{END, END, END}
		}
	},
	{
		name = '24 haluz',
		tolerance = 40,
		shape = {
			{CURVE, 400, 150},
			{CURVE, 220, 180},
			{CURVE, 330, 100},
			{CURVE, 420, 189},
			{CURVE, 120, 230},
			{LINE, 60, 90},
			{CURVE, 300, 100},
			{END, END, END}
		}
	},
	{
		name = '25 nos',
		tolerance = 27,
		shape = {
			{CURVE, 340, 70},
			{CURVE, 380, 260},
			{CURVE, 310, 220},
			{CURVE, 300, 250},
			{CURVE, 270, 220},
			{CURVE, 240, 250},
			{CURVE, 280, 70},
			{END, END, END}
		}
	},
	{
		name = '26 2 uzly',
		tolerance = 40,
		shape = {
			{CURVE, 110, 130},
			{CURVE, 150, 90},
			{CURVE, 320, 150},
			{CURVE, 330, 70},
			{CURVE, 280, 130},
			{CURVE, 240, 230},
			{CURVE, 270, 250},
			{CURVE, 300, 230},
			{CURVE, 270, 200},
			{CURVE, 230, 180},
			{END, END, END}
		}
	},
	{
		name = '27 cislo E3',
		tolerance = 33,
		shape = {
			{CURVE, 250, 120},
			{CURVE, 290, 90},
			{CURVE, 320, 120},
			{CURVE, 280, 150},
			{CURVE, 320, 180},
			{CURVE, 290, 210},
			{CURVE, 220, 180},
			{CURVE, 200, 210},
			{CURVE, 160, 180},
			{CURVE, 210, 150},
			{CURVE, 160, 120},
			{CURVE, 200, 90},
			{CURVE, 230, 110},
			{END, END, END}
		}
	},
	{
		name = '28 cislo 28',
		tolerance = 35,
		shape = {
			{CURVE, 145, 150},
			{CURVE, 170, 120},
			{CURVE, 200, 100},
			{CURVE, 200, 150},
			{CURVE, 145, 240},
			{LINE, 220, 240},
			{CURVE, 340, 130},
			{CURVE, 350, 110},
			{CURVE, 330, 90},
			{CURVE, 300, 105},
			{CURVE, 300, 145},
			{LINE, 350, 200},
			{CURVE, 350, 230},
			{CURVE, 320, 240},
			{CURVE, 290, 230},
			{CURVE, 270, 180},
			{END, END, END}
		}
	},
	{
		name = '29 pismeno B',
		tolerance = 46,
		shape = {
			{CURVE, 135, 280},
			{CURVE, 350, 50},
			{CURVE, 400, 130},
			{CURVE, 200, 140},
			{CURVE, 350, 160},
			{CURVE, 400, 240},
			{CURVE, 120, 260},
			{END, END, END}
		}
	},
	{
		name = '30 Acko',
		tolerance = 35,
		shape = {
			{CURVE, 420, 60},
			{CURVE, 420, 260},
			{CURVE, 230, 30},
			{CURVE, 270, 200},
			{CURVE, 130, 180},
			{CURVE, 390, 80},
			{END, END, END}
		}
	},
	{
		name = '31 WOW',
		tolerance = 60,
		shape = {
			{CURVE, 90, 100},
			{CURVE, 130, 200},
			{CURVE, 170, 160},
			{CURVE, 210, 200},
			{CURVE, 250, 100},
			{CURVE, 290, 80},
			{CURVE, 330, 100},
			{CURVE, 350, 140},
			{CURVE, 330, 180},
			{CURVE, 300, 200},
			{CURVE, 255, 180},
			{CURVE, 255, 120},
			{CURVE, 290, 80},
			{CURVE, 330, 80},
			{CURVE, 370, 90},
			{CURVE, 390, 100},
			{CURVE, 400, 200},
			{CURVE, 440, 160},
			{CURVE, 460, 200},
			{CURVE, 480, 100},
			{END, END, END}
		}
	},
	{
		name = '32 obdlznik a uzol',
		tolerance = 55,
		shape = {
			{CURVE, 130, 140},
			{CURVE, 230, 60},
			{CURVE, 330, 80},
			{CURVE, 350, 120},
			{CURVE, 330, 160},
			{CURVE, 300, 180},
			{CURVE, 255, 160},
			{CURVE, 255, 100},
			{CURVE, 370, 60},
			{CURVE, 420, 140},
			{LINE, 420, 200},
			{LINE, 120, 200},
			{LINE, 120, 30},
			{END, END, END}
		}
	},
	{
		name = '33 haluz',
		tolerance = 30,
		shape = {
			{CURVE, 130, 140},
			{CURVE, 230, 60},
			{CURVE, 130, 180},
			{CURVE, 350, 30},
			{LINE, 255, 160},
			{CURVE, 225, 100},
			{CURVE, 370, 160},
			{END, END, END}
		}
	},
	{
		name = '34 jablko',
		tolerance = 40,
		shape = {
			{CURVE, 227, 130},
			{CURVE, 140, 100},
			{CURVE, 124, 200},
			{CURVE, 175, 247},
			{CURVE, 230, 237},
			{CURVE, 290, 247},
			{CURVE, 336, 200},
			{CURVE, 320, 100},
			{CURVE, 240, 120},
			{LINE, 260, 70},
			{END, END, END}
		}
	},
	{
		name = '35 haluz',
		tolerance = 40,
		shape = {
			{CURVE, 132, 190},
			{CURVE, 40, 150},
			{LINE, 190, 139},
			{CURVE, 300, 220},
			{CURVE, 300, 130},
			{CURVE, 420, 250},
			{LINE, 420, 20},
			{END, END, END}
		}
	},
	{
		name = '36 cislo 40',
		tolerance = 25,
		shape = {
			{CURVE, 330, 145},
			{CURVE, 330, 180},
			{CURVE, 350, 190},
			{CURVE, 360, 160},
			{CURVE, 330, 120},
			{CURVE, 300, 180},
			{LINE, 175, 180},
			{LINE, 250, 80},
			{LINE, 250, 200},
			{CURVE, 250, 240},
			{END, END, END}
		}
	},
	{
		name = '37 ostre a tupe A',
		tolerance = 45,
		shape = {
			{CURVE, 225, 130},
			{LINE, 430, 250},
			{CURVE, 350, 30},
			{LINE, 50, 180},
			{CURVE, 250, 30},
			{CURVE, 250, 230},
			{END, END, END}
		}
	},
	{
		name = '38 hlava',
		tolerance = 25,
		shape = {
			{CURVE, 230, 240},
			{CURVE, 230, 200},
			{CURVE, 210, 198},
			{CURVE, 225, 185},
			{CURVE, 210, 175},
			{CURVE, 230, 167},
			{LINE, 230, 155},
			{LINE, 180, 155},
			{LINE, 230, 140},
			{CURVE, 230, 111},
			{CURVE, 250, 97},
			{CURVE, 270, 94},
			{CURVE, 270, 145},
			{CURVE, 260, 185},
			{CURVE, 260, 240},
			{END, END, END}
		}
	},
	{
		name = '39 o a ciara',
		tolerance = 55,
		shape = {
			{LINE, 430, 230},
			{CURVE, 190, 120},
			{CURVE, 170, 170},
			{CURVE, 170, 270},
			{CURVE, 30, 170},
			{CURVE, 170, 30},
			{CURVE, 100, 270},
			{END, END, END}
		}
	},
	{
		name = '40 haluz',
		tolerance = 35,
		shape = {
			{CURVE, 240, 270},
			{LINE, 440, 270},
			{LINE, 320, 80},
			{LINE, 200, 100},
			{LINE, 140, 170},
			{LINE, 140, 100},
			{CURVE, 100, 230},
			{CURVE, 170, 230},
			{END, END, END}
		}
	},
	{
		name = '41 strom',
		tolerance = 40,
		shape = {
			{LINE, 145, 250},
			{CURVE, 155, 150},
			{CURVE, 105, 170},
			{CURVE, 80, 120},
			{CURVE, 105, 80},
			{CURVE, 200, 75},
			{CURVE, 230, 125},
			{CURVE, 200, 170},
			{CURVE, 180, 150},
			{CURVE, 190, 250},
			{LINE, 220, 205},
			{LINE, 240, 250},
			{LINE, 260, 205},
			{LINE, 280, 250},
			{LINE, 300, 205},
			{LINE, 420, 205},
			{END, END, END}
		}
	},
	{
		name = '42 haluz',
		tolerance = 34,
		shape = {
			{CURVE, 370, 130},
			{CURVE, 370, 90},
			{CURVE, 170, 270},
			{CURVE, 200, 170},
			{CURVE, 170, 200},
			{CURVE, 390, 50},
			{CURVE, 420, 90},
			{END, END, END}
		}
	},
	{
		name = '43 usmev saso',
		tolerance = 60,
		shape = {
			{LINE, 100, 140},
			{CURVE, 260, 240},
			{CURVE, 420, 140},
			{CURVE, 260, 180},
			{CURVE, 110, 140},
			{CURVE, 170, 100},
			{CURVE, 210, 120},
			{CURVE, 250, 100},
			{CURVE, 240, 160},
			{CURVE, 270, 160},
			{CURVE, 270, 100},
			{CURVE, 310, 120},
			{CURVE, 350, 100},
			{CURVE, 400, 130},
			{END, END, END}
		}
	},
	{
		name = '44 siroka',
		tolerance = 60,
		shape = {
			{LINE, 50, 100},
			{CURVE, 80, 80},
			{CURVE, 110, 100},
			{CURVE, 40, 130},
			{CURVE, 40, 50},
			{LINE, 470, 50},
			{CURVE, 470, 220},
			{CURVE, 390, 210},
			{CURVE, 424, 180},
			{CURVE, 424, 270},
			{CURVE, 80, 270},
			{CURVE, 220, 100},
			{LINE, 370, 200},
			{END, END, END}
		}
	},
	{
		name = '45 haluz',
		tolerance = 55,
		shape = {
			{LINE, 150, 100},
			{CURVE, 180, 80},
			{CURVE, 210, 100},
			{LINE, 250, 100},
			{CURVE, 280, 80},
			{CURVE, 310, 100},
			{LINE, 350, 100},
			{CURVE, 380, 80},
			{CURVE, 410, 100},
			{LINE, 350, 200},
			{CURVE, 380, 180},
			{CURVE, 410, 200},
			{LINE, 150, 240},
			{CURVE, 180, 200},
			{CURVE, 210, 140},
			{CURVE, 210, 80},
			{CURVE, 310, 270},
			{END, END, END}
		}
	},
	{
		name = '46 daco',
		tolerance = 66,
		shape = {
			{LINE, 150, 100},
			{CURVE, 180, 280},
			{CURVE, 80, 280},
			{CURVE, 430, 180},
			{CURVE, 120, 280},
			{CURVE, 120, 80},
			{CURVE, 333, 140},
			{CURVE, 123, 240},
			{END, END, END}
		}
	},
	{
		name = '47 lod',
		tolerance = 45,
		shape = {
			{LINE, 77, 165},
			{LINE, 140, 165},
			{LINE, 140, 75},
			{LINE, 220, 75},
			{LINE, 220, 145},
			{LINE, 270, 145},
			{LINE, 270, 105},
			{LINE, 300, 105},
			{LINE, 300, 145},
			{LINE, 350, 145},
			{LINE, 350, 125},
			{LINE, 410, 125},
			{LINE, 410, 165},
			{LINE, 480, 165},
			{LINE, 440, 255},
			{LINE, 100, 255},
			{LINE, 80, 170},
			{END, END, END}
		}
	},
	{
		name = '48 haluz',
		tolerance = 36,
		shape = {
			{LINE, 80, 165},
			{LINE, 140, 165},
			{CURVE, 240, 75},
			{CURVE, 210, 145},
			{CURVE, 110, 145},
			{LINE, 280, 105},
			{CURVE, 230, 205},
			{LINE, 390, 90},
			{END, END, END}
		}
	},
	{
		name = '49 haluz osminova',
		tolerance = 65,
		shape = {
			{CURVE, 132, 90},
			{CURVE, 132, 200},
			{CURVE, 332, 200},
			{CURVE, 332, 100},
			{CURVE, 160, 150},
			{CURVE, 100, 100},
			{CURVE, 300, 130},
			{CURVE, 400, 160},
			{CURVE, 390, 80},
			{CURVE, 222, 270},
			{END, END, END}
		}
	},
	{
		name = '50 trojuholnikive blues',
		tolerance = 55,
		shape = {
			{LINE, 55, 150},
			{LINE, 105, 100},
			{LINE, 195, 150},
			{LINE, 400, 230},
			{LINE, 250, 80},
			{LINE, 150, 180},
			{LINE, 250, 150}, 
			{LINE, 200, 60},
			{LINE, 100, 260},
			{END, END, END}
		}
	},
	{
		name = '51 666',
		tolerance = 70,
		shape = {
			{CURVE, 155, 50},
			{CURVE, 105, 140},
			{CURVE, 95, 240},
			{CURVE, 125, 260},
			{CURVE, 155, 250},
			{CURVE, 175, 230},
			{CURVE, 175, 180},
			{CURVE, 145, 180},
			{CURVE, 105, 210},
			{CURVE, 86, 240},
			{CURVE, 76, 270},
			{CURVE, 196, 270},
			{CURVE, 220, 220},
			{CURVE, 250, 180},
			{CURVE, 290, 180},
			{CURVE, 290, 220},
			{CURVE, 270, 260},
			{CURVE, 250, 260},
			{CURVE, 210, 220},
			{CURVE, 210, 140},
			{CURVE, 250, 50},
			{CURVE, 380, 50},
			{CURVE, 325, 140},
			{CURVE, 315, 240},
			{CURVE, 345, 260},
			{CURVE, 375, 250},
			{CURVE, 395, 230},
			{CURVE, 395, 180},
			{CURVE, 365, 180},
			{CURVE, 325, 210},
			{CURVE, 286, 270},
			{END, END, END}
		}
	},
	{
		name = '52 polostrom',
		tolerance = 60,
		shape = {
			{LINE, 330, 50},
			{LINE, 205, 140},
			{LINE, 330, 80},
			{LINE, 205, 170},
			{LINE, 330, 110},
			{LINE, 205, 200},
			{LINE, 330, 140},
			{LINE, 205, 230},
			{LINE, 330, 170},
			{LINE, 205, 260},
			{LINE, 330, 200},
			{LINE, 205, 280},
			{LINE, 300, 280},
			{LINE, 300, 163},
			{END, END, END}
		}
	},
	{
		name = '53 slimak',
		tolerance = 70,
		shape = {
			{CURVE, 200, 150},
			{CURVE, 240, 160},
			{CURVE, 220, 180},
			{CURVE, 190, 180},
			{CURVE, 160, 150},
			{CURVE, 160, 130},
			{CURVE, 200, 110},
			{CURVE, 240, 100},
			{CURVE, 260, 130},
			{CURVE, 230, 230},
			{CURVE, 130, 200},
			{CURVE, 120, 120},
			{CURVE, 200, 80},
			{CURVE, 300, 100},
			{CURVE, 390, 250},
			{CURVE, 170, 250},
			{CURVE, 90, 250},
			{CURVE, 80, 60},
			{END, END, END}
		}
	},
	{
		name = '54 haluz',
		tolerance = 70,
		shape = {
			{CURVE, 440, 200},
			{CURVE, 160, 130},
			{CURVE, 360, 50},
			{CURVE, 90, 250},
			{CURVE, 100, 72},
			{CURVE, 300, 272},
			{CURVE, 400, 72},
			{CURVE, 200, 272},
			{END, END, END}
		}
	},
	{
		name = '55 haluz',
		tolerance = 70,
		shape = {
			{LINE, 100, 200},
			{LINE, 100, 140},
			{LINE, 150, 140},
			{LINE, 350, 100},
			{LINE, 150, 100},
			{CURVE, 250, 232},
			{CURVE, 450, 232},
			{CURVE, 450, 72}, 
			{CURVE, 400, 122},
			{CURVE, 420, 222},
			{CURVE, 300, 122},
			{CURVE, 220, 172},
			{CURVE, 120, 222},
			
			
			{END, END, END}
		}
	},
	{
		name = '56 haluz',
		tolerance = 60,
		shape = {
			{CURVE, 125, 50},
			{CURVE, 250, 100},
			{CURVE, 100, 180},
			{CURVE, 200, 200},
			{CURVE, 250, 150},
			{CURVE, 450, 250},
			{LINE, 450, 150},
			{LINE, 150, 250},
			{CURVE, 400, 50},
			{CURVE, 290, 100},
			{END, END, END}
		}
	},
	{
		name = '57 kvetina',
		tolerance = 72,
		shape = {
			{CURVE, 230, 170},
			{CURVE, 205, 220},
			{CURVE, 160, 215},
			{CURVE, 160, 175},
			{CURVE, 220, 145},
			{CURVE, 220, 125},
			{CURVE, 170, 145},
			{CURVE, 150, 125},
			{CURVE, 150, 105},
			{CURVE, 170, 75},
			{CURVE, 230, 105},
			{CURVE, 200, 35},
			{CURVE, 280, 35},
			{CURVE, 270, 105},
			{CURVE, 290, 85},
			{CURVE, 360, 55},
			{CURVE, 370, 115},
			{CURVE, 280, 121},
			{CURVE, 350, 145},
			{CURVE, 350, 185},
			{CURVE, 300, 195},
			{CURVE, 246, 145},
			{CURVE, 250, 265},
			{END, END, END}
		}
	},
	{
		name = '58 uzol',
		tolerance = 60,
		shape = {
			{CURVE, 240, 150},
			{CURVE, 205, 220},
			{CURVE, 160, 215},
			{LINE, 220, 95},
			{CURVE, 260, 145},
			{LINE, 420, 195},
			{CURVE, 290, 85},
			{CURVE, 340, 55},
			{CURVE, 350, 255},
			{LINE, 250, 85},
			{LINE, 50, 225},
			{END, END, END}
		}
	},
	{
		name = '59 haluz',
		tolerance = 65,
		shape = {
			{CURVE, 140, 50},
			{CURVE, 240, 230},
			{LINE, 380, 50},
			{LINE, 260, 130},
			{LINE, 170, 50},
			{LINE, 170, 150},
			{LINE, 370, 150},
			{LINE, 370, 260},
			{LINE, 70, 260},
			{LINE, 170, 160},
			{END, END, END}
		}
	},
	{
		name = '60 procesor',
		tolerance = 60,
		shape = {
			{LINE, 140, 70},
			{LINE, 140, 250},
			{LINE, 420, 250},
			{LINE, 420, 40},
			{LINE, 100, 40},
			{LINE, 100, 220},
			{LINE, 380, 220},
			{LINE, 380, 100},
			{LINE, 460, 100},
			{LINE, 460, 200},
			{LINE, 260, 200},
			{LINE, 260, 270},
			{END, END, END}
		}
	},
	{
		name = '61 haluz',
		tolerance = 60,
		shape = {
			{CURVE, 230, 260},
			{CURVE, 430, 160},
			{CURVE, 100, 180},
			{CURVE, 330, 30},
			{LINE, 234, 120},
			{CURVE, 305, 200},
			{CURVE, 145, 220},
			{CURVE, 370, 60},
			{END, END, END}
		}
	},
	{
		name = '62 AAA',
		tolerance = 55,
		shape = {
			{CURVE, 50, 170},
			{CURVE, 50, 70},
			{CURVE, 150, 120},
			{CURVE, 250, 220},
			{CURVE, 410, 100},
			{CURVE, 110, 200},
			{CURVE, 150, 240},
			{CURVE, 210, 80},
			{CURVE, 310, 270},
			{END, END, END}
		}
	},
	{
		name = '63 labut',
		tolerance = 45,
		shape = {
			{LINE, 300, 50},
			{LINE, 200, 50},
			{LINE, 230, 80},
			{CURVE, 140, 130},
			{LINE, 270, 90},
			{LINE, 300, 120},
			{LINE, 100, 250},
			{LINE, 390, 250},
			{LINE, 370, 200},
			{LINE, 220, 200},
			{LINE, 390, 100},
			{LINE, 300, 60},
			{END, END, END}
		}
	},
	{
		name = '64 uzol sevas',
		tolerance = 50,
		shape = {
			{LINE, 300, 50},
			{CURVE, 200, 50},
			{CURVE, 430, 180},
			{CURVE, 55, 230},
			{CURVE, 300, 120},
			{CURVE, 100, 250},
			{CURVE, 300, 60},
			{END, END, END}
		}
	},
	{
		name = '65 RUN',
		tolerance = 65,
		shape = {
			{CURVE, 140, 77},
			{CURVE, 130, 257},
			{CURVE, 80, 240},
			{CURVE, 80, 60},
			{CURVE, 200, 60},
			{CURVE, 150, 160},
			{CURVE, 200, 260},
			{CURVE, 240, 170},
			{CURVE, 260, 250},
			{CURVE, 320, 250},
			{CURVE, 340, 170},
			{CURVE, 380, 250},
			{CURVE, 400, 170},
			{CURVE, 420, 250},
			{CURVE, 430, 170},
			{CURVE, 450, 250},
			{CURVE, 470, 180},
			{END, END, END}
		}
	},
	{
		name = '66 haluz xx',
		tolerance = 50,
		shape = {
			{CURVE, 270, 80},
			{CURVE, 290, 120},
			{CURVE, 270, 160},
			{CURVE, 300, 160},
			{CURVE, 240, 130},
			{CURVE, 170, 110},
			{CURVE, 190, 150},
			{CURVE, 290, 220},
			{CURVE, 290, 270},
			{CURVE, 200, 200},
			{CURVE, 400, 240},
			{CURVE, 220, 300},
			{CURVE, 80, 230},
			{END, END, END}
		}
	},
	{
		name = '67 haluz',
		tolerance = 55,
		shape = {
			{CURVE, 300, 160},
			{LINE, 240, 130},
			{CURVE, 190, 150},
			{CURVE, 400, 240},
			{CURVE, 220, 300},
			{CURVE, 80, 230},
			{CURVE, 80, 80},
			{CURVE, 180, 160},
			{CURVE, 140, 200},
			{CURVE, 140, 100},
			{CURVE, 420, 90},
			{CURVE, 320, 190},
			{END, END, END}
		}
	},
	{
		name = '68 haluz',
		tolerance = 60,
		shape = {
			{CURVE, 180, 160},
			{CURVE, 140, 200},
			{CURVE, 240, 200},
			{CURVE, 340, 100},
			{CURVE, 400, 200},
			{CURVE, 430, 100},
			{CURVE, 460, 200},
			{CURVE, 400, 250},
			{CURVE, 400, 150},
			{CURVE, 300, 100},
			{END, END, END}
		}
	},
	{
		name = '69 haluz',
		tolerance = 60,
		shape = {
			{CURVE, 80, 160},
			{CURVE, 180, 160},
			{CURVE, 460, 200},
			{CURVE, 300, 250},
			{CURVE, 400, 150},
			{CURVE, 200, 130},
			{CURVE, 100, 230},
			{CURVE, 443, 77},
			{CURVE, 143, 77},
			{END, END, END}
		}
	},
	{
		name = '70 pentagram',
		tolerance = 50,
		shape = {
			{LINE, 270, 80},
			{LINE, 200, 230},
			{LINE, 350, 120},
			{LINE, 200, 120},
			{LINE, 350, 230},
			{LINE, 270, 50},
			{CURVE, 400, 90},
			{CURVE, 400, 230},
			{CURVE, 300, 260},
			{CURVE, 170, 260},
			{CURVE, 150, 200},
			{CURVE, 80, 100},
			{END, END, END}
		}
	},
	{
		name = '71 vlnky',
		tolerance = 65,
		shape = {
			{CURVE, 150, 150},
			{CURVE, 200, 110},
			{CURVE, 250, 150},
			{CURVE, 300, 110},
			{CURVE, 350, 150},
			{CURVE, 400, 110},
			{CURVE, 450, 130},
			{CURVE, 400, 200},
			{CURVE, 350, 130},
			{CURVE, 300, 200},
			{CURVE, 250, 130},
			{CURVE, 200, 200},
			{CURVE, 150, 130},
			{CURVE, 150, 260},
			{CURVE, 350, 60},
			{END, END, END}
		}
	},
	{
		name = '72 4 a uzol',
		tolerance = 65,
		shape = {
			{LINE, 100, 250},
			{LINE, 300, 50},
			{CURVE, 400, 160},
			{CURVE, 200, 100},
			{CURVE, 320, 110},
			{CURVE, 280, 260},
			{LINE, 100, 110},
			{LINE, 450, 250},
			{END, END, END}
		}
	},
	{
		name = '73 haluz',
		tolerance = 60,
		shape = {
			{LINE, 60, 155},
			{LINE, 240, 150},
			{CURVE, 160, 255},
			{LINE, 60, 255},
			{LINE, 220, 95},
			{CURVE, 350, 255},
			{LINE, 350, 55},
			{LINE, 230, 55},
			{CURVE, 130, 115},
			{LINE, 430, 115},
			{END, END, END}
		}
	},
	{
		name = '74 sipka dole',
		tolerance = 60,
		shape = {
			{CURVE, 140, 50},
			{CURVE, 240, 150},
			{LINE, 230, 215},
			{CURVE, 130, 115},
			{CURVE, 230, 260},
			{LINE, 430, 115},
			{LINE, 260, 200},
			{LINE, 290, 150},
			{LINE, 380, 60},
			{LINE, 280, 130},
			{LINE, 280, 50},
			{LINE, 260, 130},
			{LINE, 170, 50},
			{END, END, END}
		}
	},
	{
		name = '75 haluz',
		tolerance = 70,
		shape = {
			{CURVE, 60, 195},
			{CURVE, 240, 150},
			{CURVE, 230, 55},
			{LINE, 460, 195},
			{LINE, 460, 65},
			{CURVE, 80, 265},
			{CURVE, 50, 65},
			{LINE, 430, 265},

			{END, END, END}
		}
	},
	{
		name = '76 smutok s uchom',
		tolerance = 60,
		shape = {
			{CURVE, 430, 210},
			{CURVE, 440, 180},
			{CURVE, 470, 120},
			{CURVE, 440, 100},
			{CURVE, 406, 140},
			{CURVE, 400, 240},
			{CURVE, 250, 200},
			{CURVE, 100, 240},
			{CURVE, 170, 100},
			{CURVE, 210, 120},
			{CURVE, 250, 100},
			{CURVE, 240, 160},
			{CURVE, 270, 160},
			{CURVE, 270, 100},
			{CURVE, 310, 120},
			{CURVE, 350, 100},
			{CURVE, 400, 130},
			{END, END, END}
		}
	},
	{
		name = '77 parok s horcicu',
		tolerance = 60,
		shape = {
			{LINE, 330, 245},
			{CURVE, 330, 130},
			{CURVE, 260, 185},
			{CURVE, 190, 130},
			{CURVE, 120, 185},
			{CURVE, 100, 130},
			{CURVE, 440, 190},
			{CURVE, 440, 100},
			{CURVE, 80, 100},
			{CURVE, 80, 210},
			{CURVE, 430, 210},
			{END, END, END}
		}
	},
	{
		name = '78 Acko zlozite',
		tolerance = 60,
		shape = {
			{LINE, 310, 45},
			{CURVE, 130, 130},
			{CURVE, 360, 185},
			{CURVE, 440, 230},
			{CURVE, 340, 190},
			{CURVE, 450, 50},
			{CURVE, 330, 100},
			{CURVE, 100, 260},
			{CURVE, 330, 210},
			{CURVE, 230, 50},
			{CURVE, 130, 210},
			{END, END, END}
		}
	},
	{
		name = '79 hranovite',
		tolerance = 70,
		shape = {
			{LINE, 100, 77},
			{LINE, 300, 177},
			{LINE, 330, 245},
			{LINE, 390, 145},
			{LINE, 310, 45},
			{CURVE, 130, 150},
			{LINE, 440, 260},
			{LINE, 450, 70},
			{LINE, 230, 70},
			{CURVE, 130, 230},
			{END, END, END}
		}
	},
	{
		name = '80 END',
		tolerance = 60,
		shape = {
			{LINE, 190, 77},
			{LINE, 100, 77},
			{LINE, 100, 165},
			{LINE, 160, 180},
			{LINE, 100, 200},
			{LINE, 100, 280},
			{LINE, 190, 280},
			{LINE, 230, 77},
			{LINE, 280, 280},
			{LINE, 320, 77},
			{LINE, 330, 280},
			{CURVE, 390, 270},
			{CURVE, 420, 180},
			{CURVE, 390, 85},
			{CURVE, 330, 80},
			{END, END, END}
		}
	},
}

_M.default.admob_id = {
	['iPhone OS'] = 'ca-app-pub-1210274935500874/8865770747',
	['Android'] = 'ca-app-pub-1210274935500874/4435571140'
}

_M.default.board_id = {
	['iPhone OS'] = {
		campaign = 'campaign_top_players',
		random = 'random_line_top_players',
	},
	['Android'] = {
		campaign = 'CgkIrfSq4K4dEAIQAA', -- campaign top players
		random = 'CgkIrfSq4K4dEAIQAQ', -- random line top players
	}
}

_M.default.achievements = {
	['iPhone OS'] = {
		campaign = {
			[10] = 'archimedes',
			[20] = 'pascal',
			[30] = 'darwin',
			[40] = 'tesla',
			[50] = 'mendeleev',
			[60] = 'galileo',
			[70] = 'hawking',
			[80] = 'newton',
			[90] = 'aristoteles',
			[100] = 'einstein',
		},
	},
	['Android'] = {
		campaign = {
			[10] = 'CgkIrfSq4K4dEAIQBA',
			[20] = 'CgkIrfSq4K4dEAIQBQ',
			[30] = 'CgkIrfSq4K4dEAIQBg',
			[40] = 'CgkIrfSq4K4dEAIQBw',
			[50] = 'CgkIrfSq4K4dEAIQCA',
			[60] = 'CgkIrfSq4K4dEAIQCQ',
			[70] = 'CgkIrfSq4K4dEAIQCg',
			[80] = 'CgkIrfSq4K4dEAIQCw',
			[90] = 'CgkIrfSq4K4dEAIQDA',
			[100] = 'CgkIrfSq4K4dEAIQDQ',
		},
	},
}

_M.default.skip_options_id = {
	['Android'] = {
		['memoryline1skip'] = 'memoryline1skip',
		['memoryline5skip'] = 'memoryline5skip',
		['memoryline10skip'] = 'memoryline10skip',
		-- 'unlimited' = '',
	},
	['iPhone OS'] = {
		['memoryline1skip'] = 'org.kneego.memoryline.memoryline1skip',
		['memoryline5skip'] = 'org.kneego.memoryline.memoryline5skip',
		['memoryline10skip'] = 'org.kneego.memoryline.memoryline10skip',
		-- 'unlimited' = '',
	},
}

_M.default.skip_options_count = {
	['memoryline1skip'] = 1,
	['memoryline5skip'] = 5,
	['memoryline10skip'] = 10,
	-- 'unlimited' = '',
}

_M.default.background = {
	'1.jpg',
	'2.png',
	'3.png',
	'4.png',
	'5.png',
	'6.jpg',
	'7.jpg',
	'8.png',
	'9.png',
	'10.png',
	'11.png',
	'12.png',
	'13.png',
	'14.png',
	'15.png',
	'16.jpg',
	'17.jpg',
	'18.png',
	'19.png',
	'20.png',
	'21.jpg',
	'22.jpg',
}

return _M