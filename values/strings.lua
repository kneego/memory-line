local _M = {}

_M.default = {
	-- menu
	game_name = 'Memory line',

	play_label = 'Play',
	campaign_label = 'Campaign',
	leaderboard_label = 'Leaderboard',
	back_label = 'Back',
	menu_label = 'Menu',
	level_label = 'Level',
	levels_label = 'Levels',
	choose_level = 'Choose level',
	shop_button_label = 'Shop',

	random_play_label_1 = 'Random',
	random_play_label_2 = 'Line',

	lets_draw_label = 'Draw!',
	action_memorize = 'Memorize this line',
	action_memorize_in = ' in ',
	action_draw = 'Draw the line',
	skip_label = 'Skip this line',

	result_label = 'Result ',
	next_label = 'Next',
	try_again_label = 'Try again',

	well_done_label = {
		'Well done!',
		'Perfect!',
		'Really good!',
		'Amazing!',
		'Fine!',
		'Nice work!',
		'You are good!',
	},

	different_label = {
		'Not the same...',
		'Oh no, sorry...',
		'It looks different...',
		'Try it again!',
		'Maybe next time...',
		'Eh, one more time...',
		'Go again!',
		'Too different...',
	},

	skip_this_line = 'Skip this line?',
	really_skip_line = 'Really skip this line?',
	enter_to_shop = 'You do not have available skip option. Enter to the shop?',
	yes = 'Yes',
	no = 'No',

	-- shop
	shop_label = 'Memory line shop',
	buy_label_1 = 'Buy',
	buy_label_2 = 'skip',
	please_wait = 'Please wait...',
	number_of_skip = 'Current number of skip options: ',
}

_M.sk = {
	-- menu
	game_name = 'Memory line',

	play_label = 'Hraj',
	campaign_label = 'Kampaň',
	leaderboard_label = 'Najlepší hráči',
	back_label = 'Späť',
	menu_label = 'Menu',
	level_label = 'Úroveň',
	levels_label = 'Úrovne',
	choose_level = 'Vyber si úroveň',
	shop_button_label = 'Obchod',

	random_play_label_1 = 'Náhodné',
	random_play_label_2 = 'Čiary',

	lets_draw_label = 'Kresli!',
	action_memorize = 'Zapamätaj si túto čiaru',
	action_memorize_in = ' za ',
	action_draw = 'Kresli čiaru',
	skip_label = 'Preskoč túto čiaru',

	result_label = 'Výsledok ',
	next_label = 'Ďalej',
	try_again_label = 'Skúsiť znova',

	well_done_label = {
		'Perfektné!',
		'Paráda!',
		'Naozaj dobré!',
		'Super!',
		'Pekne!',
		'Dobrá práca!',
		'Si super!',
	},

	different_label = {
		'Nie sú rovnaké...',
		'Oh toho nie, prepáč...',
		'Vyzerajú rozdielne...',
		'Skús to ešte raz!',
		'Možno ďalsí pokus?',
		'Eh, tak znova...',
		'Poď znova!',
		'Príliš rôzne...',
	},

	skip_this_line = 'Preskočiť túto čiaru?',
	really_skip_line = 'Naozaj chceš preskočiť túto čiaru?',
	enter_to_shop = 'Nemáš už žiadne preskočenie. Chceš navštíviť obchod?',
	yes = 'Áno',
	no = 'Nie',

	-- shop
	shop_label = 'Memory line obchod',
	buy_label_1 = 'Kúp',
	buy_label_2 = 'preskoč',
	please_wait = 'Prosím čakaj...',
	number_of_skip = 'Aktuálny počet preskočení: ',
}

_M.cz = {
    -- menu
    game_name = 'Memory line',

    play_label = 'Hraj',
    campaign_label = 'Kampaň',
    leaderboard_label = 'Nejlepší hráči',
    back_label = 'Zpět',
    menu_label = 'Menu',
    level_label = 'Úroveň',
    levels_label = 'Úrovně',
    choose_level = 'Vyber si úroveň',
    shop_button_label = 'Obchod',

    random_play_label_1 = 'Náhodná',
    random_play_label_2 = 'Čára',

    lets_draw_label = 'Kresli!',
    action_memorize = 'Zapamatuj si tuto čáru',
    action_memorize_in = ' za ',
    action_draw = 'Kresli čáru',
    skip_label = 'Přeskoč tuto čáru',

    result_label = 'Výsledek ',
    next_label = 'Další',
    try_again_label = 'Skusit znovu',

    well_done_label = {
        'Perfektní!',
        'Paráda!',
        'Skutečně dobré!',
        'Supr!',
        'Hezky!',
        'Skvělá práce!',
        'Si supr!',
    },

    different_label = {
        'Nejsou stejné..',
        'Oh toto ne, promiň...',
        'Vypadají rozdílně...',
        'Skus to ješte jednou!',
        'Možná další pokus?',
        'Eh, tak znova...',
        'Pojď znova!',
        'Příliš různé...',
    },

    skip_this_line = 'Přeskočit tuto čáru?',
    really_skip_line = 'Opravdu chceš přeskočit tuto čáru?',
    enter_to_shop = 'Nemáš již žádné přeskočení. Chceš navštívit obchod?',
    yes = 'Ano',
    no = 'Ne',

    -- shop
    shop_label = 'Memory line obchod',
    buy_label_1 = 'Kup',
    buy_label_2 = 'přeskoč',
    please_wait = 'Prosím čekej...',
    number_of_skip = 'Aktuální počet přeskočení: ',
}

return _M