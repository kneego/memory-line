# Memory Line

Full source code and assets of Memory Line game for [Android](https://play.google.com/store/apps/details?id=org.kneego.memory_line) and [iOS](https://itunes.apple.com/us/app/memory-line/id974980772?ls=1&mt=8).

The game uses the [Corona SDK](https://coronalabs.com) game engine.
