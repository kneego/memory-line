--
-- Infinario - game analytics module
--

local os = require( 'os' )
local json = require( 'json' )
local system = require( 'system' )
local network = require( 'network' )
local display = require( 'display' )

local version = 1.0

local function networkListener(event)
	if event.isError then
		print ('Infinario request failed.')
	end
end

local _M = {}

function _M:initialize(project_id)
	self.project_id = project_id
	self.user_id = nil

	self.debug = false

	self.appName = system.getInfo( 'appName' )
	self.appVersion = system.getInfo( 'appVersionString' )
	self.cid = system.getInfo( 'deviceID' )

	self.ua = 'Infinario/' .. version .. ' (Corona; U; ' ..
		system.getInfo( 'platformName' ) .. ' ' .. system.getInfo( 'platformVersion' ) .. '; ' ..
		system.getPreference( 'locale', 'identifier' ) .. '; ' ..
		system.getInfo( 'platformVersion' ) .. ')'

	self.headers = {
		['User-Agent'] = self.ua,
		['Content-Type'] = 'application/json'
	}

	self.sr = display.pixelWidth .. 'x' .. display.pixelHeight
	self.vp = screenW .. 'x' .. screenH
end

function _M:identify(user_id)
	self.user_id = user_id
end

function _M:track(event_type, params)
	local properties = {
		v = version,
		an = self.appName,
		av = self.appVersion,
		ua = self.ua,
		sr = self.sr,
		vp = self.vp,
	}

	if params ~= nil and type(params) == 'table' then
		for k, v in pairs( params ) do
			properties[k] = v
		end
	end

	local data = {
		['type'] = event_type,
		customer_ids = { registered = self.user_id, cookie = self.cid},
		properties = properties,
	}

	self:sendData( '/crm/events', data )
end

function _M:update(params)
	local properties = {}

	if params ~= nil and type(params) == 'table' then
		for k, v in pairs( params ) do
			properties[k] = v
		end
	end

	local data = {
		ids = { registered = self.user_id, cookie = self.cid},
		properties = properties,
	}

	self:sendData( '/crm/customers', data )
end

function _M:sendData( endpoint, data )
	data.project_id = self.project_id
	data.timestamp = os.time()
	
	local json_data = json.encode( data )

	if self.debug then
		print( 'Infinario JSON data:', json_data )
	else
		network.request(
			'http://api.infinario.com' .. endpoint,
			'POST',
			networkListener,
			{
				body = json_data,
				headers = self.headers
			}
		)
	end
end

return _M