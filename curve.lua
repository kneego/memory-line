local table = require( 'table' )

local _M = {}

function _M.draw( points, tension, number_of_segments, draw_func )
	if points == nil or #points < 2 then
		return
	end

	-- skonvertujeme zoznam bodov na format, aky potrebujeme
	local _points = {}
	for i = 1, #points do
		table.insert( _points, points[i].x )
		table.insert( _points, points[i].y )
	end
	points = _points

	local tension = tension or 0.5
	local number_of_segments = number_of_segments or 25

	local pts = table.copy( points )
	local l = #points
	local r_pos = 1
	local r_len = (l - 2) * number_of_segments + 2
	local res = {} -- vysleden pole, pocet prvkov = r_len
	local cache = {} -- cache pre vypocet, pocet prvkov = (number_of_segments + 2) * 4 = 108

	local cache_ptr = 5 -- indexing from 1

	-- copy 1. point and insert at beginning
	table.insert( pts, 1, points[2] )
	table.insert( pts, 1, points[1] )

	-- duplicate end-points
	table.insert( pts, points[#points - 1] )
	table.insert( pts, points[#points] )

	-- cache inner-loop calculations as they are based on t alone
	cache[1] = 1; -- 1,0,0,0

	for i = 1, number_of_segments - 1 do
		local st = i / number_of_segments
		local st2 = st * st
		local st3 = st2 * st
		local st23 = st3 * 2
		local st32 = st2 * 3

		cache[cache_ptr] = st23 - st32 + 1 -- c1
		cache[cache_ptr + 1] = st32 - st23 -- c2
		cache[cache_ptr + 2] = st3 - 2 * st2 + st -- c3
		cache[cache_ptr + 3] = st3 - st2 -- c4

		cache_ptr = cache_ptr + 4
	end

	cache_ptr = cache_ptr + 1
	cache[cache_ptr] = 1 -- 0,1,0,0

	-- we use local variables here
	function parse(pts, cache, l)
		local t

		local i = 3
		while i < l do
			local pt1 = pts[i]
			local pt2 = pts[i + 1]
			local pt3 = pts[i + 2]
			local pt4 = pts[i + 3]

			local t1x = (pt3 - pts[i - 2]) * tension
			local t1y = (pt4 - pts[i - 1]) * tension
			local t2x = (pts[i + 4] - pt1) * tension
			local t2y = (pts[i + 5] - pt2) * tension

			for t = 0, number_of_segments - 1 do
				local c = t * 4

				local c1 = cache[c + 1] or 0
				local c2 = cache[c + 2] or 0
				local c3 = cache[c + 3] or 0
				local c4 = cache[c + 4] or 0

				res[r_pos] = c1 * pt1 + c2 * pt3 + c3 * t1x + c4 * t2x
				res[r_pos + 1] = c1 * pt2 + c2 * pt4 + c3 * t1y + c4 * t2y

				r_pos = r_pos + 2
			end

			i = i + 2
		end
	end

	parse(pts, cache, l)

	-- add last point
	res[r_pos] = points[#points - 1]
	res[r_pos + 1] = points[#points]

	-- add lines to path
	local i = 1
	if draw_func ~= nil then
		while i < #res do
			draw_func(res[i], res[i + 1])

			i = i + 2
		end
	end

	return res
end

return _M