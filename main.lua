-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

require( 'knee.globals' )
require( 'common' )

-- data = require( "data" )
ads = require( 'ads' )
utils = require( 'knee.utils' )
back_button = require( 'knee.back_button' )

display.setStatusBar( display.HiddenStatusBar )
display.setDefault( 'background', 1, 1, 1 )
display.setDefault( 'fillColor', utils.color(153, 153, 153) )

debugging = true

if debugging then
	require( 'knee.screen_capture' )
end

infinario = require( 'knee.infinario' )
infinario:initialize('1887d31c-c349-11e4-b774-b083fedeed2e')
infinario.debug = false

game_network = require( 'knee.game_network' )
game_network.debugging = false

game_network.load_local_player_callback = function (event)
	if event ~= nil and event.data ~= nil then
		infinario:identify( event.data.playerID )

		infinario:update({
			alias = event.data.alias,
		})
	end
end

game_network.init()

-- rate dialog
rate = require( 'knee.rate' )
rate.init({
	android_rate = 'market://details?id=org.kneego.memory_line',
	ios_rate = 'itms://itunes.apple.com/us/app/memory-line/id974980772?mt=8',
	times_used = 10,
	days_used = 5,
	version = app_version,
	remind_times = 5,
	remind_days = 10,
	rate_title = 'Rate Memory Line',
	rate_text = 'If you enjoy this game, please take a moment to rate it. Thank you for your support!',
	rate_button = 'Rate now',
	remind_button = 'Remind later',
	cancel_button = 'No, thanks',
})

-- resources (strings, arrays)
R = require( 'resources' )
sounds = require('sounds')

ga = require( 'knee.ga' )
ga:init( 'UA-39791763-22' )

-- reklamu zobraujeme inu pre iOS a inu pre Android
ads.init( 'admob', R.arrays('admob_id')[platform_name])

back_button.init()

-- load settings
settings = utils.loadSettings({
	-- default empty data
	emptyData = {
		version = app_version,
		vibrations = true,
		sounds = true,
		music = true,
		game_play_counter = 0,
		selected_category = 1,
		selected_difficulty = 2,
		skip_options = 3,
	}
})

-- inicializacia levelov podla definicie obrazkov
if settings['levels'] == nil then settings['levels'] = {} end

for i = 1, #R.arrays('levels') do
	if settings['levels'][i] == nil then
		-- novy level
		settings['levels'][i] = {
			unlocked = false,
			score = 0
		}

		if i == 1 then
			settings['levels'][i].unlocked = true
		end
	end
end

-- fall back koli starym verziam settingov
if settings.skip_options == nil then
	-- default mame 3x skip
	settings.skip_options = 3
end

-- ulozime settingy
utils.saveSettings(settings)

local composer = require( 'composer' )

composer.gotoScene( 'menu' )
