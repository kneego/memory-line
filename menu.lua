-----------------------------------------------------------------------------------------
--
-- menu.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

local system = require( 'system' )
local native = require('native')
local table = require( 'table' )

local curve = require( 'curve' )

local sceneGroup
local sound_icon
local game_name


function show_sound_icon()
	if sound_icon ~= nil then
		sound_icon:removeSelf( )
		sound_icon = nil
	end

	if settings.sounds then
		sound_icon = display.newImageRect( 'images/volumeon.png', 45, 36 )
	else
		sound_icon = display.newImageRect( 'images/volumeoff.png', 45, 36 )
	end

	sound_icon.y = screenH - 50
	sound_icon.x = halfW + 10
	sound_icon.anchorX = 0
	sceneGroup:insert( sound_icon )

	utils.handlerAdd( sound_icon, 'touch', function ( self, event )
		if event.phase == 'began' then
			button_down( sound_icon )
		elseif event.phase == 'ended' then
			if sound_icon.down then
				if settings.sounds then
					settings.sounds = false
				else
					settings.sounds = true
				end

				ga:event('toogle menu', 'enabled', settings.sounds)
				infinario:track('toogle sound', {enabled = settings.sounds})

				show_sound_icon()

				utils.saveSettings( settings )

				button_up( sound_icon )
			end
		end

		return true
	end)
end

function scene:create( event )
	sceneGroup = self.view

	local bg = display.newImageRect( 'images/bg/1.jpg', screenW, screenH )
	bg.x = halfW
	bg.y = halfH
	sceneGroup:insert( bg )

	game_name = display.newText( {
		text = R.strings('game_name'),
		fontSize = 34,
		font = 'SimplyRounded',
		x = halfW,
		y = 0
	} )
	sceneGroup:insert( game_name )

	-- play
	local play_button_group = display.newGroup( )
	sceneGroup:insert( play_button_group )

	local play_button = display.newImageRect( 'images/play.png', 125, 125 )
	play_button.x = halfW
	play_button.y = halfH
	play_button_group:insert( play_button )

	utils.handlerAdd(play_button_group, 'touch', function ( self, event )
		if event.phase == 'began' then
			button_down( play_button_group )
		elseif event.phase == 'ended' and play_button_group.down then
			button_up( play_button_group )

			ga:view('select level')
			infinario:track('screen', {name = 'select level'})

			goto_scene( 'level', 250 )

			return true
		end
	end )

	local play_label = display.newText( {
		text = R.strings('play_label'),
		fontSize = 34,
		font = 'SimplyRounded',
		x = halfW,
		y = halfH - 10
	} )
	play_label:setFillColor( 1 )
	play_button_group:insert( play_label )

	local campaign_label = display.newText( {
		text = R.strings('campaign_label'),
		fontSize = 14,
		font = 'SimplyRounded',
		x = halfW,
		y = halfH + 15
	} )
	campaign_label:setFillColor( 1 )
	play_button_group:insert( campaign_label )

	-- play random
	local random_play_button_group = display.newGroup( )
	sceneGroup:insert( random_play_button_group )

	local random_play_button = display.newImageRect( 'images/level-current.png', 81, 81 )
	random_play_button.x = halfW + 130
	random_play_button.y = halfH + 21
	random_play_button_group:insert( random_play_button )

	utils.handlerAdd(random_play_button_group, 'touch', function ( self, event )
		if event.phase == 'began' then
			button_down( random_play_button_group )
		elseif event.phase == 'ended' and random_play_button_group.down then
			button_up( random_play_button_group )

			goto_scene( 'game', 250, {
				params = {
					level = nil,
				}
			})

			return true
		end
	end )

	local random_play_label_1 = display.newText( {
		text = R.strings('random_play_label_1'),
		fontSize = 14,
		font = 'SimplyRounded',
		x = halfW + 130,
		y = halfH - 8 + 21
	} )
	random_play_label_1:setFillColor( 1 )
	random_play_button_group:insert( random_play_label_1 )

	local random_play_label_2 = display.newText( {
		text = R.strings('random_play_label_2'),
		fontSize = 14,
		font = 'SimplyRounded',
		x = halfW + 130,
		y = halfH + 8 + 21
	} )
	random_play_label_2:setFillColor( 1 )
	random_play_button_group:insert( random_play_label_2 )

	-- shop
	local shop_button_group = display.newGroup( )
	sceneGroup:insert( shop_button_group )

	local shop_button = display.newImageRect( 'images/shop.png', 81, 81 )
	shop_button.x = halfW - 130
	shop_button.y = halfH + 21
	shop_button_group:insert( shop_button )

	utils.handlerAdd(shop_button_group, 'touch', function ( self, event )
		if event.phase == 'began' then
			button_down( shop_button_group )
		elseif event.phase == 'ended' and shop_button_group.down then
			button_up( shop_button_group )

			goto_scene( 'game_store', 250, {
				params = {
					level = nil,
				}
			} )

			return true
		end
	end )

	local shop_label = display.newText( {
		text = R.strings('shop_button_label'),
		fontSize = 14,
		font = 'SimplyRounded',
		x = halfW - 130,
		y = halfH + 21
	} )
	shop_label:setFillColor( 1 )
	shop_button_group:insert( shop_label )

	-- leaderboard
	local leaderboard_icon = display.newImageRect( 'images/leader.png', 31, 41 )
	-- leaderboard_icon.y = screenH - 50
	-- leaderboard_icon.anchorX = 0
	leaderboard_icon.anchorX = 1
	leaderboard_icon.x = halfW - 10
	leaderboard_icon.y = screenH - 50
	sceneGroup:insert( leaderboard_icon )

	-- local leaderboard_label = display.newText( {
	-- 	text = R.strings('leaderboard_label'),
	-- 	fontSize = 18,
	-- 	font = 'SimplyRounded',
	-- 	y = screenH - 50
	-- } )
	-- leaderboard_label.anchorX = 1
	-- sceneGroup:insert( leaderboard_label )

	-- -- nastavime spravne pozicie
	-- local leaderboard_width = leaderboard_label.width + 10 + leaderboard_icon.width
	-- leaderboard_icon.x = halfW - (leaderboard_width / 2)
	-- leaderboard_label.x = halfW + (leaderboard_width / 2)

	-- local leaderboard_button = display.newRect( halfW, screenH - 50, leaderboard_width, 50 )
	-- leaderboard_button.alpha = 0
	-- leaderboard_button.isHitTestable = true
	-- sceneGroup:insert( leaderboard_button )
	
	utils.handlerAdd(leaderboard_icon, 'touch', function ( self, event )
		if event.phase == 'began' then
			button_down( leaderboard_icon )
		elseif event.phase == 'ended' and leaderboard_icon.down then
			button_up( leaderboard_icon )

			ga:view('leaderboard')
			infinario:track('screen', {name = 'leaderboard'})

			game_network.show_leaderboards()

			return true
		end
	end )
end

function scene:show( event )
	sceneGroup = self.view

	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
		
		-- sounds button
		show_sound_icon()
	elseif phase == "did" then
		-- Called when the scene is now on screen
		-- 
		-- INSERT code here to make the scene come alive
		get_label( game_name )

		ga:view('menu')
		infinario:track('screen', {name = 'menu'})
	end	
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
	elseif phase == "did" then
		-- Called when the scene is now off screen
		display.remove( sound_icon )
		sound_icon = nil
	end	
end

function scene:destroy( event )
	local sceneGroup = self.view
	
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene