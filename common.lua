-- common methods
local composer = require( 'composer' )

function goto_scene( scene, delay, params )
	if delay == nil then delay = 0 end

	timer.performWithDelay( delay, function()
		composer.gotoScene( scene, params )
	end )
end

function button_down( button )
	button.y = button.y + 2
	button.down = true

	display.currentStage:setFocus( button )

	sounds.play(sounds.sound.button_click)
end

function button_up( button )
	button.y = button.y - 2
	button.down = false

	display.currentStage:setFocus( nil )

	sounds.play(sounds.sound.button_click)
end

function get_label( target )
	target.y = 0
	
	transition.to( target, {
		y = 50,
		time = 400,
		transition = easing.outBack
	} )
end