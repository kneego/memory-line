-----------------------------------------------------------------------------------------
--
-- menu.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

local os = require( 'os' )
local system = require( 'system' )
local native = require('native')
local table = require( 'table' )
local math = require( 'math' )

local curve = require( 'curve' )
local gesture = require('gesture')

-- definicia konstant
local END = 1000
local LINE = 1001
local CURVE = 1002

local lets_draw_group
local sceneGroup -- hlavny kontainer
local bg -- touch listener
local bg_image -- background image sa bude menit nahodne
local selected_background -- ktory background zobrazujeme

local different_lite_group_container
local different_lite_group_background

local well_done_lite_group_container
local well_done_lite_group_background

local well_done_group_container
local well_done_group_background

local skip_group
local preview_group
local shape_object

local points_table = {} -- body po ktorych sa hrac pohybuje
local line -- hrac kresli ciaru
local level -- cislo levelu
local current_level -- aktualny level
local try_count -- ak presiahne urcity pocet, zobrazime skip button
local game_count -- pri random kategorii zaciname od lahsich tvarov
local random_game_score -- score pri random line

local back_label
local back_button
local show_ad = false

local preview_shape_points -- tuto budu body vykreslenej ciary. Budeme podla nich urcovat minimalnu dlzku
local current_gesture_fingerprint_1 = '' -- toto hrac bude hadat
local current_gesture_fingerprint_2 = '' -- otoceny fingerprint

local timer_counter
local hide_timer -- skovava preview
local pump_timer -- timer na pulzujuci text ze kreslit treba

local touch_listener_installed = false
local touch_listener_phase = false
local level_text -- obsahuje clislo levelu
local action_text -- obsahuje aktualnu hlasku pre to, co ma hrac robit

local well_done_group
local well_done_text -- obsahuje hlasku o uspesnom ukonceni levelu
local well_done_text_zoom -- ako vyssie, iba sa bude zoomovat
local result_text -- obsahuje dosiahnute percenta
local result_text_zoom -- ako vyssie, iba sa bude zoomovat
local score_text -- obsahuje dosiahnute score
local different_text -- obsahuje hlasku o neuspesnom pokuse
local different_text_zoom -- ako vyssie, iba sa bude zoomovat

local well_done_lite_group
local well_done_lite_text -- obsahuje hlasku o uspesnom ukonceni levelu
local well_done_lite_result_text -- obsahuje dosiahnute percenta

local different_lite_group

local pump_action_label = false
local pump_phase_small = true


function show_score(score)
	score_text.text = 'Score: ' .. score
	score_text.x = screenW - 10
end

function percent_to_score( percent )
	local _score = 0

	if percent >= 90 then
		_score = 3
	elseif percent >= 80 then
		_score = 2
	elseif percent >= 70 then
		_score = 1
	end

	return _score
end

function calculate_campaign_score()
	local _score = 0

	for i = 1, #settings.levels do
		local _level_score = settings.levels[i].score
		_score = _score + percent_to_score(_level_score)
	end

	return _score
end

function draw_line()
	-- najprv sa vyhodi stara kresba
	remove_shape()
	
	local numPoints = #points_table
	local nl = {}
	local j, p
		 
	nl[1] = points_table[1]
		 
	j = 2
	p = 1
		 
	for i = 2, numPoints, 1  do
		nl[j] = points_table[i]
		j = j + 1
		p = i 
	end
	
	if ( p  < numPoints -1 ) then
		nl[j] = points_table[numPoints - 1]
	end

	if #nl > 2 then
		add_to_shape( nl, R.arrays('settings').line_width, R.arrays('settings').line_color, true )
	end
end

function check_solution()
	-- skovame skip button
	skip_group.alpha = 0

	-- zvysime counter hier
	settings.game_play_counter = settings.game_play_counter + 1
	utils.saveSettings( settings )

	local reversed_points_table = {}
	for i = 1, #points_table do
		reversed_points_table[i] = points_table[#points_table - i + 1]
	end

	local _match_result = {
		gesture.get_match_score(current_gesture_fingerprint_1, points_table, preview_shape_points, current_level.tolerance),
		gesture.get_match_score(current_gesture_fingerprint_2, points_table, preview_shape_points, current_level.tolerance),

		-- a este porovname otocene gesto
		gesture.get_match_score(current_gesture_fingerprint_1, reversed_points_table, preview_shape_points, current_level.tolerance),
		gesture.get_match_score(current_gesture_fingerprint_2, reversed_points_table, preview_shape_points, current_level.tolerance),
	}

	-- -1 trocha znizuje presnost
	local _match = math.min(unpack(_match_result)) - 1

	-- aby nam sedeli score pozicie
	if _match < 1 then _match = 1 end

	-- zvysime pocitadlo pokusov
	try_count = try_count + 1

	-- zvysime pocitadlo hier
	game_count = game_count + 1

	ga:event('end game', 'level', level or 'random shape')
	ga:event('end game', 'match', _match)
	infinario:track('end game', {
		level = level or 'random shape', 
		match = _match,
		try_count = try_count,
		points = #current_level.shape - 1,
		game_count = game_count,
	})

	-- a mame tu reklamu!
	if settings.store_used ~= nil then
		if (settings.game_play_counter % 15 == 0) or show_ad then
			show_ad = true
			if ads.isLoaded("interstitial") then
				show_ad = false
				ads.show( "interstitial", { x=0, y=0, testMode = false } )
			end
		end
	end

	if _match < 4 then
		-- povedzme, ze je to ok
		sounds.play(sounds.sound.success)

		-- vyhodime staru kresbu
		remove_shape_success()

		-- vypocitame percenta
		local _score = {
			90,
			80,
			70,
		}

		-- aaaaaaaaaa konstanta!
		_percent = _score[_match] + math.random( 9 )

		if level ~= nil then
			-- campaign

			-- nastavime score
			utils.debug(settings['levels'][level].score)
			if settings['levels'][level].score < _percent then
				settings['levels'][level].score = _percent
			end

			if level < #settings['levels'] then
				-- odomnkem dalsi level
				settings['levels'][level + 1].unlocked = true
			end

			-- submitnem score
			local _campaign_score = calculate_campaign_score()

			if is_emulator then
				utils.debug('check_solution:send score', 'campaign', _campaign_score)
			else
				game_network.send_score(R.arrays('board_id')[platform_name].campaign, _campaign_score)

				-- este achievements odomknem
				local _achievement = R.arrays('achievements')[platform_name].campaign[level]
				if _achievement then
					-- ok, tento level odomyka achievement
					game_network.unlock_achievement(_achievement)
				end
			end

			show_score(_campaign_score)

			-- a ulozime settingy
			utils.saveSettings(settings)

			-- zobrazime well done spravu
			local _text = 'Level ' .. level .. '. ' .. R.strings('well_done_label')[math.random(#R.strings('well_done_label'))]
			well_done_text.text = _text
			well_done_text_zoom.text = _text

			well_done_text_zoom.x = well_done_text.x + well_done_text.width * 0.5
			well_done_text_zoom.xScale = 1
			well_done_text_zoom.yScale = 1
			well_done_text_zoom.alpha = 1
			transition.to( well_done_text_zoom, {
				time = 800,
				xScale = 1.4,
				yScale = 1.4,
				alpha = 0
			} )

			_text = R.strings('result_label') .. _percent .. '%'
			result_text.text = _text
			result_text_zoom.text = _text

			result_text_zoom.x = result_text.x + result_text.width * 0.5
			result_text_zoom.xScale = 1
			result_text_zoom.yScale = 1
			result_text_zoom.alpha = 1
			transition.to( result_text_zoom, {
				time = 800,
				xScale = 1.4,
				yScale = 1.4,
				alpha = 0
			} )

			-- zobrazime spravne pozadie
			if well_done_group_background ~= nil then
				well_done_group_background:removeSelf()
			end

			well_done_group_background = display.newImageRect( 'images/bg/' .. selected_background, screenW, screenH )
			well_done_group_background.x = halfW
			well_done_group_background.y = halfH
			well_done_group_container:insert( well_done_group_background )

			well_done_group.alpha = 1
		else
			-- random shape

			-- submitnem score
			random_game_score = random_game_score + percent_to_score(_percent)

			if is_emulator then
				utils.debug('check_solution:send score', 'random', random_game_score)
			else
				game_network.send_score(R.arrays('board_id')[platform_name].random, random_game_score)
			end

			show_score(random_game_score)

			-- zobrazime well done lite screen
			well_done_lite_text.text = R.strings('well_done_label')[math.random(#R.strings('well_done_label'))]
			well_done_lite_result_text.text = R.strings('result_label') .. _percent .. '%'

			-- zobrazime spravne pozadie
			if well_done_lite_group_background ~= nil then
				well_done_lite_group_background:removeSelf()
			end

			well_done_lite_group_background = display.newImageRect( 'images/bg/' .. selected_background, screenW, screenH )
			well_done_lite_group_background.x = halfW
			well_done_lite_group_background.y = halfH
			well_done_lite_group_container:insert( well_done_lite_group_background )

			well_done_lite_group.alpha = 1

			timer.performWithDelay( 1500, function ( ... )
				well_done_lite_group.alpha = 0

				start_game()
			end )
		end

		-- resetneme counter pokusov
		try_count = 0

		-- zobrazime dalsie pozadie
		selected_background = R.arrays('background')[math.random(1, 22)]

		-- aby sa zobrazil rate dialog v rozumnu dobu
		rate.rate()
	else
		local _text = R.strings('different_label')[math.random(#R.strings('different_label'))]
		different_text.text = _text

		different_text_zoom.text = _text
		different_text_zoom.xScale = 1
		different_text_zoom.yScale = 1
		different_text_zoom.alpha = 1
		transition.to( different_text_zoom, {
			time = 800,
			xScale = 1.4,
			yScale = 1.4,
			alpha = 0
		} )

		-- je to na hovno
		sounds.play(sounds.sound.fail)

		-- zobrazime spravne pozadie
		if different_lite_group_background ~= nil then
			different_lite_group_background:removeSelf()
		end

		different_lite_group_background = display.newImageRect( 'images/bg/' .. selected_background, screenW, screenH )
		different_lite_group_background.x = halfW
		different_lite_group_background.y = halfH
		different_lite_group_container:insert( different_lite_group_background )

		-- try it again screen
		different_lite_group.alpha = 1

		timer.performWithDelay( 1000, function ( ... )
			different_lite_group.alpha = 0

			show_preview()
		end )
	end
end

function touch_listener(self, event)
	if "began" == event.phase then
		touch_listener_phase = true

		points_table = {}

		local pt = {}
		pt.x = event.x
		pt.y = event.y

		table.insert(points_table, pt)
	
	elseif "moved" == event.phase then
		if touch_listener_phase == true then
			local pt = {}
			pt.x = event.x
			pt.y = event.y

			table.insert(points_table, pt)

			draw_line()
		end
	
	elseif "ended" == event.phase or "cancelled" == event.phase then
		if touch_listener_phase == true then
			touch_listener_phase = false

			utils.handlerRemove(bg, 'touch')
			touch_listener_installed = false
			
			draw_line()

			check_solution()
		end
	end
end

-- function add_to_shape( shape_points, width, color )
-- 	if shape_object == nil then
-- 		shape_object = display.newLine( shape_points[1].x, shape_points[1].y, shape_points[2].x, shape_points[2].y)

-- 		shape_object:setStrokeColor(unpack( color ))
-- 		shape_object.strokeWidth = width

-- 		for i = 3, #shape_points do 
-- 			shape_object:append( shape_points[i].x, shape_points[i].y )
-- 		end
-- 	else
-- 		for i = 1, #shape_points do
-- 			shape_object:append( shape_points[i].x, shape_points[i].y )
-- 		end
-- 	end
-- end

-- function remove_shape( )
-- 	if shape_object ~= nil then
-- 		shape_object:removeSelf( )
-- 		shape_object = nil
-- 	end
-- end

function add_to_shape( shape_points, width, color )
	if shape_object == nil then
		shape_object = display.newLine( shape_points[1].x, shape_points[1].y, shape_points[2].x, shape_points[2].y)

		shape_object:setStrokeColor(unpack( color ))
		shape_object.strokeWidth = width

		for i = 3, #shape_points do 
			shape_object:append( shape_points[i].x, shape_points[i].y )
		end
	else
		for i = 1, #shape_points do
			shape_object:append( shape_points[i].x, shape_points[i].y )
		end
	end
end

function remove_shape()
	if shape_object ~= nil then
		shape_object:removeSelf( )
		shape_object = nil
	end
end

function effect_shape(shape, color, frequency)
	if color == nil then color = { 0, 0, 0 } end
	if frequency == nil then frequency = 1 end

	-- points table obsahuje jednotlive vrcholy
	for _, point in ipairs( points_table ) do
		if math.random(frequency) < 2 then
			-- prvotna valkost
			local _start_size = math.random( 3 ) + 2

			-- ako sa bude zvacsovat
			local _end_size = _start_size + math.random(5) + 2

			local c
			if shape == 'circle' then
				-- nakreslime kruh
				c = display.newCircle( point.x, point.y, _start_size)
			elseif shape == 'rect' then
				c = display.newRect( point.x, point.y, _start_size, _start_size)
				c.rotation = math.random( 360 )
				c.strokeWidth = _start_size
				c:setStrokeColor( 0 )
				c:setFillColor( 1 )

				if math.random( 2 ) > 1 then
					c:setFillColor( 0 )
				end

				_end_size = _end_size * 4
			elseif shape == 'triangle' then
				c = display.newPolygon(point.x, point.y, { 0,0 , 0,_start_size*2 , _start_size*2,0 })
				c.rotation = math.random( 360 )
				c.strokeWidth = 2
				c:setStrokeColor( 0 )
				c:setFillColor( 1 )

				if math.random( 2 ) > 1 then
					c:setFillColor( 0 )
				end
			elseif shape == 'star' then
				c = display.newPolygon(point.x, point.y, { 0,-110, 27,-35, 105,-35, 43,16, 65,90, 0,45, -65,90, -43,15, -105,-35, -27,-35 } )
				c.width = _start_size * 4
				c.height = _start_size * 4

				c.rotation = math.random( 360 )
				c.strokeWidth = math.random( 4 ) + 1
				c:setStrokeColor( utils.color(unpack(color)) )
				c:setFillColor( 1 )

				if math.random( 2 ) > 1 then
					c:setFillColor( utils.color(unpack(color)) )
				end

				_end_size = _start_size * 60
			else
				return
			end

			c.alpha = 0.7 + (math.random( 3 ) / 10)
			
			transition.to( c, {
				x = point.x + math.random( 10 ) - 5,
				y = point.y + math.random( 10 ) - 5,
				width = _end_size,
				height = _end_size,
				rotation = math.random( 360 ),
				alpha = 0 + (math.random( 4 ) / 10),
				time = 300 + math.random( 200 ),
				onComplete = function( o )
					o:removeSelf( )
				end
			} )
		end
	end
end

function effect_circle()
	effect_shape('circle')
end

function effect_rect()
	effect_shape('rect')
end

function effect_triangle()
	effect_shape('triangle')
end

function effect_star()
	effect_shape('star')
end

function effect_sparcle()
	-- points table obsahuje jednotlive vrcholy
	for _, point in ipairs( points_table ) do
		local _count = math.random( 8 ) + 5
		while (_count > 0) do
			-- prvotna valkost
			local _radius = math.random( 2 )

			-- nakreslime kruh
			local c = display.newCircle( point.x, point.y, _radius)
			c.alpha = 0.7 + (math.random( 3 ) / 10)
			
			transition.to( c, {
				x = point.x + math.random( 40 ) - 20,
				y = point.y + math.random( 40 ) - 20,
				alpha = 0 + (math.random( 4 ) / 10),
				time = 200 + math.random( 100 ),
				onComplete = function( o )
					o:removeSelf( )
				end
			} )

			_count = _count - 1
		end
	end
end

function effect_cloud()
	-- points table obsahuje jednotlive vrcholy
	for _, point in ipairs( points_table ) do
		-- prvotna valkost
		local _radius = math.random( 4 ) + 7
		-- nakreslime kruh
		local c = display.newCircle( point.x, point.y, _radius)
		c.alpha = 0.7 + (math.random( 3 ) / 10)
		c:setFillColor( 0.9 )

		-- podla aktualnej velkosti urcujeme dalsie hodnoty
		local _middle_size = (math.random(10) / 10) + 1
		local _end_size = _middle_size - (math.random(3) / 10)
		if _end_size < 0 then _end_size = 0 end
		utils.debug(c.width, _middle_size, _end_size)
		
		transition.to( c, {
			xScale = _middle_size,
			yScale = _middle_size,
			time = 100 + math.random( 100 ),
			transition = easing.inOutQuad,
			onComplete = function( o )
				transition.to( o, {
					xScale = _end_size,
					yScale = _end_size,
					time = 400 + math.random( 100 ),
					alpha = 0.3 + (math.random( 3 ) / 10),
					transition = easing.inOutQuad,
					onComplete = function( o )
						o:removeSelf( )
					end
				} )
			end
		} )
	end
end

function effect_rotation()
	transition.to( shape_object, {
		rotation = math.random( 4000 ) - 2000,
		alpha = 0,
		time = 600,
		xScale = 0,
		yScale = 0,
		transition = easing.inCirc,
		onComplete = function()
			if shape_object ~= nil then
				shape_object:removeSelf( )
				shape_object = nil
			end
		end
	} )
end

function remove_shape_fail()
	if shape_object ~= nil then
		local _effect = math.random( 8 )

		if _effect == 1 then
			transition.to( shape_object, {
				y = shape_object.y + 4,
				alpha = 0,
				time = 400,
				strokeWidth = 6,
				onComplete = function()
					if shape_object ~= nil then
						shape_object:removeSelf( )
						shape_object = nil
					end
				end
			} )
		elseif _effect == 2 then
			shape_object:removeSelf( )
			shape_object = nil

			effect_circle()
		elseif _effect == 3 then
			shape_object:removeSelf( )
			shape_object = nil

			effect_rect()
		elseif _effect == 4 then
			shape_object:removeSelf( )
			shape_object = nil

			effect_triangle()
		elseif _effect == 5 then
			shape_object:removeSelf( )
			shape_object = nil

			effect_star()
		elseif _effect == 6 then
			shape_object:removeSelf( )
			shape_object = nil

			effect_sparcle()
		elseif _effect == 7 then
			shape_object:removeSelf( )
			shape_object = nil

			effect_cloud()
		elseif _effect == 8 then
			effect_rotation()
		end
	end

	points_table = {}
end

function remove_shape_success( )
	if shape_object ~= nil then
		shape_object:setStrokeColor(utils.color(255, 215, 0))
		transition.to( shape_object, {
			y = shape_object.y - 15,
			alpha = 0,
			time = 800,
			strokeWidth = 14,
			onComplete = function()
				if shape_object ~= nil then
					shape_object:removeSelf( )
					shape_object = nil
				end
			end
		} )

		effect_shape('star', {255, 215, 0}, 3)
	end

	points_table = {}
end

function calculate_fingerprint(shape_points, tolerance)
	-- mame nakreslene, vypocitame fingerprint pre tento tvar
	local current_gesture_fingerprint_1 = gesture.get_fingerprint(shape_points, tolerance)

	-- a este aj opacne gesto si vypocitame
	local all_shape_points_reversed = {}
	local total_count = #shape_points
	for i = 1, total_count do
		all_shape_points_reversed[i] = shape_points[total_count - i + 1]
	end

	local current_gesture_fingerprint_2 = gesture.get_fingerprint(all_shape_points_reversed, tolerance)

	return current_gesture_fingerprint_1, current_gesture_fingerprint_2
end

function draw_shape(shape_definition)
	local all_shape_points = {} -- pouzije sa pre vypocet fingerprintu
	local shape_points = {} -- pouzije sa pre vykreslovanie

	-- start
	table.insert(shape_points, {x = shape_definition[1][2], y = shape_definition[1][3]})
	table.insert(all_shape_points, {x = shape_definition[1][2], y = shape_definition[1][3]})

	-- prvy objekt
	local last_shape = shape_definition[1][1]

	-- utils.debug('preview_group', preview_group)

	for i = 2, #shape_definition do
		local _type, _x, _y = shape_definition[i][1], shape_definition[i][2], shape_definition[i][3]

		if _type ~= last_shape then
			-- ukoncujeme sled objektov
			if last_shape == LINE then
				-- ciara
				-- pouzijem existujucu metodu na krivky
				for j = 1, #shape_points do
					-- startovacia pozicia
					local old_x = shape_points[j].x
					local old_y = shape_points[j].y

					-- pre kazde dva body kreslim ciaru
					curve.draw({ shape_points[j], shape_points[j + 1] }, nil, nil, function ( _x, _y )
						-- pridame tieto body do preview objektu
						local _line = display.newLine( old_x, old_y, _x, _y )
						preview_group:insert(_line)

						_line:setStrokeColor(unpack( R.arrays('settings').preview_line_color ))
						_line.strokeWidth = R.arrays('settings').preview_line_width

						table.insert(all_shape_points, {x = _x, y = _y})

						old_x = _x
						old_y = _y
					end)
				end

			elseif last_shape == CURVE then
				local old_x = shape_points[1].x
				local old_y = shape_points[1].y

				curve.draw(shape_points, nil, nil, function ( _x, _y )
					-- pridame tieto body do preview objektu
					local _line = display.newLine( old_x, old_y, _x, _y )
					preview_group:insert(_line)

					_line:setStrokeColor(unpack( R.arrays('settings').preview_line_color ))
					_line.strokeWidth = R.arrays('settings').preview_line_width

					table.insert(all_shape_points, {x = _x, y = _y})

					old_x = _x
					old_y = _y
				end)
			end

			-- prvy bod je ten, co bol teraz posledny
			local old_x = shape_points[#shape_points].x
			local old_y = shape_points[#shape_points].y

			-- vynulujeme zoznam bodov
			shape_points = {}

			-- pridame novu startovaciu poziciu
			table.insert(shape_points, {x = old_x, y = old_y})

			-- nastavime novy typ objektu
			last_shape = _type

			-- pridavame bod do definicie
			table.insert(shape_points, {x = _x, y = _y})
		else
			-- pridavame bod do definicie
			table.insert(shape_points, {x = _x, y = _y})
		end

		-- pre debugovanie
		-- aby sme videli zadefinovane body
		-- local _circle = display.newCircle( _x, _y, 5 )
		-- preview_group:insert(_circle)
	end

	return all_shape_points
end

function random_shape()
	local _points = {}

	local _limit = game_count / 10 + 1
	if _limit > 5 then limit = 5 end

	local _count_of_points = math.random(_limit) + 2

	local _tolerance = _count_of_points * 5
	if _tolerance < 20 then
		_tolerance = 20
	elseif _tolerance > 50 then
		_tolerance = 50
	end

	for i = 1, _count_of_points do
		table.insert(_points, {CURVE, math.random( screenW - 80 ) + 40, math.random( screenH - 80 ) + 40})
	end

	table.insert(_points, {END, END, END})

	current_level = {
		name = '',
		tolerance = _tolerance,
		shape = _points,
	}
end

function start_drawing()
	-- skovame lets draw button
	lets_draw_group.alpha = 0

	-- zobrazime skip button
	show_skip_button()

	-- skovame preview
	if preview_group ~= nil then
		preview_group:removeSelf( )
		preview_group = nil
	end

	-- povolime touch handler aby hrac mohol kreslit
	utils.handlerAdd(bg, 'touch', touch_listener)
	touch_listener_installed = true

	action_text.text = R.strings('action_draw')
	
	pump_action_label = true
	pump()
end

function pump()
	local _scale
	
	if not pump_action_label then
		return
	end

	if pump_phase_small then
		pump_phase_small = false
		_scale = 1.06
	else
		pump_phase_small = true
		_scale = 1
	end

	transition.to( action_text, {
		xScale = _scale,
		yScale = _scale,
		time = 300,
		onComplete = pump
	} )
end

function show_skip_button()
	-- pod 3 pokusy nerob nic
	if level == nil and try_count < 3 then return end

    -- toto je uz posledny level, nema kam skipnut
	if level ~= nil and level == #settings['levels'] then return end

	-- zobrazime mu skip button
	skip_group.alpha = 1
end

function hide_timer_handler()
	pump_action_label = false

	if pump_timer ~= nil then
		timer.cancel( pump_timer )
	end

	if timer_counter < 1 then
		start_drawing()

		-- reklama prec
		ads.hide()

		-- reklamu naloadujeme
		if settings.store_used ~= nil then
			ads.load( "interstitial", { testMode=false } )
		end
	else
		action_text.text = R.strings('action_memorize') .. R.strings('action_memorize_in') .. timer_counter
		action_text.xScale = 1.06
		action_text.yScale = 1.06

		transition.to( action_text, {
			xScale = 1,
			yScale = 1,
			time = 300,
		} )

		-- ideme na dalsi tick
		hide_timer = timer.performWithDelay( 700, hide_timer_handler)
	end

	timer_counter = timer_counter - 1
end

function show_preview()
	-- pustam hru
	ga:event('start game', 'level', level or 'random shape')
	infinario:track('start game', {level = level or 'random shape'})

	sounds.play(sounds.sound.draw)

	action_text.text = R.strings('action_memorize') .. R.strings('action_memorize_in') .. '3'

	-- vyhodime staru kresbu
	remove_shape_fail()

	-- sem sa supne preview
	preview_group = display.newGroup( )

	preview_shape_points = draw_shape( current_level.shape )

	-- mame nakreslene, vypocitame fingerprint pre tento tvar
	current_gesture_fingerprint_1, current_gesture_fingerprint_2 = calculate_fingerprint(preview_shape_points, current_level.tolerance)

	utils.debug('draw_shape:current_gesture_fingerprint', current_gesture_fingerprint_1, current_gesture_fingerprint_2)

	-- pocitadlo zostavajuceho casu
	timer_counter = 3

	-- preview nechame zobrazene a potom skovame
	hide_timer_handler()

	-- zobrazime lets draw button
	lets_draw_group.alpha = 1

	-- skovame screeny, ktore teraz nepotrebujeme
	skip_group.alpha = 0
	well_done_group.alpha = 0
	well_done_lite_group.alpha = 0
	different_lite_group.alpha = 0
end

function start_game()
	if bg_image ~= nil then
		bg_image:removeSelf()
	end

	bg_image = display.newImageRect( 'images/bg/' .. selected_background, screenW, screenH )
	bg_image.x = halfW
	bg_image.y = halfH
	bg:insert(bg_image)

	if level == nil then
		random_shape()

		level_text.text = ''
	else
		current_level = R.arrays('levels')[level]

		level_text.text = R.strings('level_label') .. ' ' .. level .. '.'
	end

	show_preview()
end

function stop_game()
	-- odstranime touch handler
	if touch_listener_installed then
		utils.handlerRemove(bg, 'touch')
		touch_listener_installed = false
	end

	-- vyhodim timer
	if hide_timer ~= nil then
		timer.cancel( hide_timer ) -- zrusime timer odpocitavania
		hide_timer = nil
	end

	-- skovame aktualne preview
	if preview_group ~= nil then
		preview_group:removeSelf( )
		preview_group = nil
	end
	
	try_count = 0
end

function lets_draw_handler( self, event )
	-- skovame preview a ideme kreslit
	if event.phase == 'began' then
		button_down( lets_draw_group )
	elseif event.phase == 'ended' and lets_draw_group.down then
		button_up( lets_draw_group )

		-- skovame lets draw button
		lets_draw_group.alpha = 0
		
		timer.cancel( hide_timer ) -- zrusime timer odpocitavania

		start_drawing()	
	end

	return true
end

function skip_callback( event )
	utils.debug('game:skip_callback', event.action, event.index)

	-- ak to nie je click, tak nic nerobime
	if event.action ~= 'clicked' then return end

    -- tlacidlo Yes
    if event.index == 1 then
    	-- zistim, ci este existuju dalsie levely
    	-- ak nie, nic neurobime
    	if level >= #settings['levels'] then return end

    	-- zopar dat do analytik
    	ga:event('skip', 'try_count', try_count)
		ga:event('skip', 'points', #current_level.shape - 1)

		infinario:track('skip', {
			try_count = try_count,
			points = #current_level.shape - 1,
		})

		level = level + 1

    	-- zistim, ci dalsi level uz nie je odomknuty
    	if settings['levels'][level].unlocked then
    		-- ak hej, tak iba prepnem na dalsi bez znizenia skip kreditu
    	else
    		-- odomknem level
	    	settings['levels'][level].unlocked = true

	    	-- znizim skip o jednu
			settings.skip_options = settings.skip_options - 1
			utils.saveSettings( settings )
		end

		-- vycistime hraciu plochu
		stop_game()

		-- start next game
		start_game()
	end
end

function show_store_callback( event )	
	-- ak to nie je click, tak nic nerobime
	if event.action ~= 'clicked' then return end

    -- tlacidlo Yes
    if event.index == 1 then
    	composer.gotoScene( 'game_store' )
    end
end

function build_skip_group()
	-- skip button
	skip_group = display.newGroup( )
	skip_group.alpha = 0
	sceneGroup:insert(skip_group)

	local skip_icon = display.newImageRect( 'images/skip.png', 27, 24 )
	skip_icon.anchorX = 0
	skip_icon.anchorY = 1
	skip_icon.y = screenH - 10
	skip_icon.x = screenW - 37
	skip_group:insert( skip_icon )

	local skip_label = display.newText( {
		text = R.strings('skip_label'),
		fontSize = 20,
		font = 'SimplyRounded',
		x = 0,
		y = screenH - 10
	} )
	skip_label.anchorX = 0
	skip_label.anchorY = 1
	skip_group:insert( skip_label )

	-- nastavim pozicie
	local _width =  skip_label.width + 5 + 37
	skip_label.x = screenW - _width

	local skip_button = display.newRect( screenW - _width, screenH - 10, _width, 24 )
	skip_button.anchorX = 0
	skip_button.anchorY = 1
	skip_button.alpha = 0
	skip_button.isHitTestable = true
	skip_group:insert( skip_button )

	utils.handlerAdd(skip_group, 'touch', function ( self, event )
		if event.phase == 'began' then
			button_down( skip_group )
		elseif event.phase == 'ended' and skip_group.down then
			button_up( skip_group )

			-- ak ide o campaign
			if level ~= nil then
				if settings.skip_options > 0 then
					native.showAlert( R.strings('skip_this_line'), R.strings('really_skip_line'), { R.strings('yes'), R.strings('no') }, skip_callback )
				else
					native.showAlert( R.strings('skip_this_line'), R.strings('enter_to_shop'), { R.strings('yes'), R.strings('no') }, show_store_callback )
				end
			else
				-- znizime score
				random_game_score = random_game_score - 1
				if random_game_score < 0 then random_game_score = 0 end

				ga:event('skip', 'try_count', try_count)
				ga:event('skip', 'points', #current_level.shape - 1)
				infinario:track('skip', {
					try_count = try_count,
					points = #current_level.shape - 1,
				})

				show_score(random_game_score)

				-- vycistime hraciu plochu
				stop_game()

				-- start next game
				start_game()
			end

			return true
		end
	end )
end

function build_well_done_group()
	well_done_group = display.newGroup( )
	well_done_group.alpha = 0

	sceneGroup:insert(well_done_group)

	well_done_group_container = display.newGroup()
	well_done_group:insert( well_done_group_container )

	utils.handlerAdd(well_done_group_container, 'touch', function ( ... )
		return true
	end)

	-- zobrazime well done ikonku
	local well_done_icon = display.newImageRect( well_done_group, 'images/check.png', 64, 51 )
	well_done_icon.anchorX = 0
	well_done_icon.y = halfH

	well_done_text = display.newText( {
		text = R.strings('well_done_label')[1],
		fontSize = 28,
		font = 'SimplyRounded',
		x = 0,
		y = halfH - 16,
	} )
	well_done_text.anchorX = 0
	well_done_group:insert( well_done_text )

	well_done_text_zoom = display.newText( {
		text = R.strings('well_done_label')[1],
		fontSize = 28,
		font = 'SimplyRounded',
		x = well_done_text.x + well_done_text.width * 0.5,
		y = halfH - 16,
	} )
	well_done_group:insert( well_done_text_zoom )

	-- nastavime poziciu
	local _width = (61 + 10 + well_done_text.width) / 2
	well_done_icon.x = halfW - _width
	well_done_text.x = halfW - _width + 71
	well_done_text_zoom.x = halfW - _width + 71

	-- result text
	result_text = display.newText( {
		text = '',
		fontSize = 28,
		font = 'SimplyRounded',
		x = halfW - _width + 61 + 10, -- vedla ikonky
		y = halfH + 16
	} )
	result_text.anchorX = 0
	result_text:setFillColor( utils.color(45, 125, 50) )
	well_done_group:insert( result_text )

	result_text_zoom = display.newText( {
		text = '',
		fontSize = 28,
		font = 'SimplyRounded',
		x = halfW - _width + 61 + 10, -- vedla ikonky
		y = halfH + 16
	} )
	result_text_zoom:setFillColor( utils.color(45, 125, 50) )
	well_done_group:insert( result_text_zoom )

	-- try again button
	local try_again_button_group = display.newGroup( )
	well_done_group:insert( try_again_button_group )

	local try_again_icon = display.newImageRect( 'images/tryagain.png', 30, 35 )
	try_again_icon.anchorX = 0
	try_again_icon.anchorY = 1
	try_again_icon.y = screenH - 5	
	try_again_button_group:insert( try_again_icon )

	local try_again_label = display.newText( {
		text = R.strings('try_again_label'),
		fontSize = 20,
		font = 'SimplyRounded',
		x = screenW - 36,
		y = screenH - 10
	} )
	try_again_label.anchorX = 0
	try_again_label.anchorY = 1
	try_again_button_group:insert( try_again_label )

	local try_again_button = display.newRect( screenW - 10, screenH - 5, try_again_label.width + 40, 35 )
	try_again_button.anchorX = 0
	try_again_button.anchorY = 1
	try_again_button.alpha = 0
	try_again_button.isHitTestable = true
	try_again_button_group:insert( try_again_button )

	-- nastavim spravne pozicie
	local _width = (30 + 10 + try_again_label.width) / 2
	try_again_icon.x = halfW - _width
	try_again_label.x = halfW - _width + 40
	try_again_button.x = halfW - _width

	utils.handlerAdd(try_again_button_group, 'touch', function ( self, event )
		if event.phase == 'began' then
			button_down( try_again_button_group )
		elseif event.phase == 'ended' and try_again_button_group.down then
			button_up( try_again_button_group )

			-- start game again
			timer.performWithDelay( 250, function()
				start_game()
			end )

			return true
		end
	end )

	-- next button
	local next_button_group = display.newGroup( )
	well_done_group:insert( next_button_group )

	local next_icon = display.newImageRect( 'images/next.png', 16, 24 )
	next_icon.anchorX = 1
	next_icon.anchorY = 1
	next_icon.y = screenH - 10
	next_icon.x = screenW - 10
	next_button_group:insert( next_icon )

	local next_label = display.newText( {
		text = R.strings('next_label'),
		fontSize = 20,
		font = 'SimplyRounded',
		x = screenW - 36,
		y = screenH - 10
	} )
	next_label.anchorX = 1
	next_label.anchorY = 1
	next_button_group:insert( next_label )

	local next_button = display.newRect( screenW - 10, screenH - 10, next_label.width + 36, 24 )
	next_button.anchorX = 1
	next_button.anchorY = 1
	next_button.alpha = 0
	next_button.isHitTestable = true
	next_button_group:insert( next_button )

	utils.handlerAdd(next_button_group, 'touch', function ( self, event )
		if event.phase == 'began' then
			button_down( next_button_group )
		elseif event.phase == 'ended' and next_button_group.down then
			button_up( next_button_group )

			if touch_listener_installed then
				utils.handlerRemove(bg, 'touch')
				touch_listener_installed = false
			end
			
			if level < #settings['levels'] then
				-- odomnkem dalsi level
				level = level + 1
			end

			-- start next game
			timer.performWithDelay( 250, function()
				start_game()
			end )

			return true
		end
	end )

	-- back button
	local back_button_group = display.newGroup( )
	sceneGroup:insert( back_button_group )

	local back_icon = display.newImageRect( back_button_group, 'images/back.png', 16, 24 )
	back_icon.anchorX = 0
	back_icon.anchorY = 1
	back_icon.x = 10
	back_icon.y = screenH - 10

	back_label = display.newText( {
		text = '',
		fontSize = 20,
		font = 'SimplyRounded',
		x = 36,
		y = screenH - 10
	} )
	back_label.anchorX = 0
	back_label.anchorY = 1
	back_button_group:insert( back_label )

	back_button = display.newRect( 10, screenH - 10, back_label.width + 36, 24 )
	back_button.anchorX = 0
	back_button.anchorY = 1
	back_button.alpha = 0
	back_button.isHitTestable = true
	back_button_group:insert( back_button )

	utils.handlerAdd(back_button_group, 'touch', function ( self, event )
		if event.phase == 'began' then
			button_down( back_button_group )
		elseif event.phase == 'ended' and back_button_group.down then
			button_up( back_button_group )

			if level ~= nil then
				composer.gotoScene( 'level' )
			else
				composer.gotoScene( 'menu' )
			end

			return true
		end
	end )
end

function build_well_done_lite_group()
	well_done_lite_group = display.newGroup( )
	well_done_lite_group.alpha = 0

	sceneGroup:insert(well_done_lite_group)

	well_done_lite_group_container = display.newGroup()
	well_done_lite_group:insert( well_done_lite_group_container )

	utils.handlerAdd(well_done_lite_group_container, 'touch', function ( ... )
		return true
	end)

	-- zobrazime well done ikonku
	local well_done_icon = display.newImageRect( well_done_lite_group, 'images/check.png', 64, 51 )
	well_done_icon.anchorX = 0
	well_done_icon.y = halfH

	well_done_lite_text = display.newText( {
		text = R.strings('well_done_label')[1],
		fontSize = 28,
		font = 'SimplyRounded',
		x = 0,
		y = halfH - 16,
	} )
	well_done_lite_text.anchorX = 0
	well_done_lite_group:insert( well_done_lite_text )

	-- nastavime poziciu
	local _width = (61 + 10 + well_done_lite_text.width) / 2
	well_done_icon.x = halfW - _width
	well_done_lite_text.x = halfW - _width + 71

	-- result text
	well_done_lite_result_text = display.newText( {
		text = '',
		fontSize = 28,
		font = 'SimplyRounded',
		x = halfW - _width + 61 + 10, -- vedla ikonky
		y = halfH + 16
	} )
	well_done_lite_result_text.anchorX = 0
	well_done_lite_result_text:setFillColor( utils.color(45, 125, 50) )
	well_done_lite_group:insert( well_done_lite_result_text )
end

function build_different_lite_group()
	different_lite_group = display.newGroup( )
	--different_lite_group.alpha = 0

	sceneGroup:insert(different_lite_group)

	different_lite_group_container = display.newGroup()
	different_lite_group:insert( different_lite_group_container )

	utils.handlerAdd(different_lite_group_container, 'touch', function ( ... )
		return true
	end)

	different_text = display.newText( {
		text = '',
		fontSize = 28,
		font = 'SimplyRounded',
		x = halfW,
		y = halfH - 16
	} )
	different_text:setFillColor( utils.color(193, 39, 45) )
	different_lite_group:insert( different_text )

	different_text_zoom = display.newText( {
		text = '',
		fontSize = 28,
		font = 'SimplyRounded',
		x = halfW,
		y = halfH - 16
	} )
	different_text_zoom:setFillColor( utils.color(193, 39, 45) )
	different_lite_group:insert( different_text_zoom )
end

function scene:create( event )
	sceneGroup = self.view

	-- background
	-- bg = display.newRect( halfW, halfH, screenW, screenH )
	-- bg:setFillColor( 1 )

	bg = display.newGroup()
	sceneGroup:insert( bg )

	-- level label
	level_text = display.newText( {
		text = '',
		fontSize = 20,
		font = 'SimplyRounded',
		x = 10,
		y = 10
	} )
	level_text.anchorX = 0
	level_text.anchorY = 0
	sceneGroup:insert( level_text )

	-- action (memorize / draw)
	action_text = display.newText( {
		text = '',
		fontSize = 20,
		font = 'SimplyRounded',
		x = halfW,
		y = 10
	} )
	action_text.anchorY = 0
	action_text:setFillColor( utils.color(193, 39, 45) )
	sceneGroup:insert( action_text )

	-- score
	score_text = display.newText( {
		text = '',
		fontSize = 20,
		font = 'SimplyRounded',
		x = 0,
		y = 10
	} )
	score_text.anchorX = 1
	score_text.anchorY = 0
	sceneGroup:insert( score_text )

	-- back button
	local back_button_group = display.newGroup( )
	sceneGroup:insert( back_button_group )

	local back_icon = display.newImageRect( back_button_group, 'images/back.png', 16, 24 )
	back_icon.anchorX = 0
	back_icon.anchorY = 1
	back_icon.x = 10
	back_icon.y = screenH - 10

	back_label = display.newText( {
		text = '',
		fontSize = 20,
		font = 'SimplyRounded',
		x = 36,
		y = screenH - 10
	} )
	back_label.anchorX = 0
	back_label.anchorY = 1
	back_button_group:insert( back_label )

	back_button = display.newRect( 10, screenH - 10, back_label.width + 36, 24 )
	back_button.anchorX = 0
	back_button.anchorY = 1
	back_button.alpha = 0
	back_button.isHitTestable = true
	back_button_group:insert( back_button )

	utils.handlerAdd(back_button_group, 'touch', function ( self, event )
		if event.phase == 'began' then
			button_down( back_button_group )
		elseif event.phase == 'ended' and back_button_group.down then
			button_up( back_button_group )

			if level ~= nil then
				composer.gotoScene( 'level' )
			else
				composer.gotoScene( 'menu' )
			end

			return true
		end
	end )

	lets_draw_group = display.newGroup()
	sceneGroup:insert( lets_draw_group )

	local lets_draw_icon = display.newImageRect( lets_draw_group, 'images/level-current.png', 81, 81 )
	lets_draw_icon.x = screenW - 50
	lets_draw_icon.y = screenH - 50

	local lets_draw_label = display.newText( {
		text = R.strings('lets_draw_label'),
		fontSize = 20,
		font = 'SimplyRounded',
		x = screenW - 50,
		y = screenH - 50
	} )
	lets_draw_label:setFillColor( 1 )
	lets_draw_group:insert( lets_draw_label )

	local lets_draw_button = display.newRect( screenW - 91, screenH - 91, 81, 81 )
	lets_draw_button.alpha = 0
	lets_draw_button.isHitTestable = true
	lets_draw_group:insert( lets_draw_button )

	utils.handlerAdd(lets_draw_group, 'touch', lets_draw_handler)

	build_skip_group()
	build_well_done_group()
	build_well_done_lite_group()
	build_different_lite_group()
end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
		if event.params then 
			-- init levelu
			level = event.params.level
		end

		try_count = 0 -- pocitadlo pokusov
		game_count = 0 -- pocitadlo odohranych hier
		random_game_score = 0

		if level == nil then
			back_label.text = R.strings('menu_label')

			-- zobrazime score
			show_score(random_game_score)
		else
			back_label.text = R.strings('levels_label')

			-- zobrazime score
			show_score(calculate_campaign_score())
		end

		back_button.width = back_label.width + 36

		-- pustime muziku
		if settings.sounds then
			audio.play( sounds.sound.music, {channel = sounds.channel.MUSIC_CHANNEL, loops = -1} )
			audio.setVolume( 0.15, {channel = sounds.channel.MUSIC_CHANNEL} )
		end
	elseif phase == "did" then
		-- Called when the scene is now on screen
		-- 
		-- INSERT code here to make the scene come alive

		ga:view('menu')
		infinario:track('screen', {name = 'game'})

		selected_background = R.arrays('background')[math.random(1, 22)]
		start_game()
	end	
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		if settings.music then
			audio.fadeOut( sounds.channel.MUSIC_CHANNEL, 1500 )
		end

	elseif phase == "did" then
		-- Called when the scene is now off screen
		-- remove_shape( group_user_draw )
		-- remove_shape( group_shape_preview )

		stop_game()
	end	
end

function scene:destroy( event )
	local sceneGroup = self.view
	
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene