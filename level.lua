-----------------------------------------------------------------------------------------
--
-- menu.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

local native = require('native' )
local system = require( 'system' )
local widget = require( 'widget' )

local last_position = 0
local level_selected = nil

local last_unlocked_position
local levels_scroll_widget
local choose_level
local pressed_button  -- ak stlacim button a pohnem scroll view

function levels_scroll_widget_handler( event )
	if event ~= nil then
		if event.phase == 'began' then
			-- ulozim aktualnu poziciu
			local x, y = levels_scroll_widget:getContentPosition()
			last_position = x
		elseif event.phase == 'ended' then
			-- ak je stlaceny button
			if pressed_button ~= nil then
				button_up( pressed_button )
				level_selected = pressed_button._level
				pressed_button = nil
			end

			-- overim o kolko sa scroll posunul
			local x, y = levels_scroll_widget:getContentPosition()
			if x > last_position + 5 or x < last_position - 5 then
				-- je mimo, tak level nepustam
				level_selected = nil
			elseif level_selected ~= nil then
				-- ok, pustim level
				ga:event('campaign', 'level', level_selected)
				infinario:track('screen', {name = 'campaign', level = level_selected})

				goto_scene( 'game', 250, {
					params = {
						level = level_selected,
					}
				} )

				level_selected = nil

				return true
			end
		end
	end
end

function scene:create( event )
	local sceneGroup = self.view

	local bg = display.newImageRect( 'images/bg/1.jpg', screenW, screenH )
	bg.x = halfW
	bg.y = halfH
	sceneGroup:insert( bg )

	-- choose level
	choose_level = display.newText( {
		text = R.strings('choose_level'),
		fontSize = 34,
		font = 'SimplyRounded',
		x = halfW,
		y = 50
	} )
	sceneGroup:insert( choose_level )

	-- back button
	local back_button_group = display.newGroup( )
	sceneGroup:insert( back_button_group )

	local back_icon = display.newImageRect( back_button_group, 'images/back.png', 16, 24 )
	back_icon.anchorX = 0
	back_icon.anchorY = 1
	back_icon.x = 10
	back_icon.y = screenH - 10

	local back_label = display.newText( {
		text = R.strings('menu_label'),
		fontSize = 20,
		font = 'SimplyRounded',
		x = 36,
		y = screenH - 10
	} )
	back_label.anchorX = 0
	back_label.anchorY = 1
	back_button_group:insert( back_label )

	local back_button = display.newRect( 10, screenH - 10, back_label.width + 36, 24 )
	back_button.anchorX = 0
	back_button.anchorY = 1
	back_button.alpha = 0
	back_button.isHitTestable = true
	back_button_group:insert( back_button )
	
	utils.handlerAdd(back_button_group, 'touch', function ( self, event )
		if event.phase == 'began' then
			button_down( back_button_group )
		elseif event.phase == 'ended' and back_button_group.down then
			button_up( back_button_group )

			goto_scene( 'menu', 250 )
		end

		return true
	end )
end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
		last_position = 0
		level_selected = nil

		-- zoznam s levelmi
		local _level_count = #settings['levels'] -- pocet levelov
		local _item_width = 81 + 65
		local _width = _level_count * 81 + (_level_count - 1) * 65 + 100

		-- scroll view s levelmi
		levels_scroll_widget = widget.newScrollView( {
			left = 0,
			top = halfH - 40,
			scrollHeight = 83,
			scrollWidth = _width,
			height = 83,
			width = screenW,
			verticalScrollDisabled = true,
			rightPadding = 100,
			listener = levels_scroll_widget_handler,
			hideBackground = true,
		} )
		sceneGroup:insert( levels_scroll_widget )

		-- sem budeme scrollovat usera
		last_unlocked_position = 1

		local _position = 50
		local _view = levels_scroll_widget:getView()
		for i = 1, _level_count do
			local level_button_group = display.newGroup( )
			levels_scroll_widget:insert( level_button_group )

			local level_button

			if settings['levels'][i].unlocked then
				-- toto je posledny odomnkuty level a sem sa budeme scrollovat
				last_unlocked_position = _position + 40

				if settings['levels'][i].score == 0 then
					-- toto je level, ktory este nebol hrany. teda je to ten aktualny level
					level_button = display.newImageRect( 'images/level-current.png', 81, 81 )
					level_button_group:insert( level_button )

					local level_number = display.newText( {
						text = i .. '.',
						fontSize = 34,
						font = 'SimplyRounded',
						x = _position + 42,
						y = 36
					} )
					level_number:setFillColor( 1, 1, 1 )
					level_button_group:insert( level_number )

					level_button_group._level = i
				else
					-- toto je uz odohrany level
					level_button = display.newImageRect( 'images/level-done.png', 81, 81 )
					level_button_group:insert( level_button )

					local level_number = display.newText( {
						text = i .. '.',
						fontSize = 14,
						font = 'SimplyRounded',
						x = _position + 40,
						y = 18
					} )
					level_number:setFillColor( 1, 1, 1 )
					level_button_group:insert( level_number )

					level_button_group._level = i

					local level_score = display.newText( {
						text = settings['levels'][i].score .. '%',
						fontSize = 10,
						font = 'SimplyRounded',
						x = _position + 40,
						y = 63
					} )
					level_score:setFillColor( 1, 1, 1 )
					level_button_group:insert( level_score )
				end

				utils.handlerAdd(level_button_group, 'touch', function ( self, event )
					utils.debug('button', event.phase)
					if event.phase == 'began' then
						button_down( level_button_group )
						pressed_button = level_button_group
					elseif event.phase == 'ended' and level_button_group.down then
						button_up( level_button_group )
						pressed_button = nil
						level_selected = i
					end
				end )
			else
				-- zamknuty level
				level_button = display.newImageRect( 'images/level-locked.png', 81, 81 )
				levels_scroll_widget:insert( level_button )
			end

			level_button.anchorX = 0
			level_button.x = _position
			level_button.y = 40

			if i < _level_count then
				local line = display.newImageRect( 'images/line-' .. math.random( 1, 5 ) .. '.png', 65, 9 )
				line.anchorX = 0
				line.x = _position + 81
				line.y = 40
				levels_scroll_widget:insert( line )
			end

			_position = _position + _item_width
		end

	elseif phase == "did" then
		-- Called when the scene is now on screen
		-- 
		-- INSERT code here to make the scene come alive
		get_label( choose_level )

		ga:view('level')
		infinario:track('screen', {name = 'level'})

		-- nascrollujeme usera na posledny odomnkuty level
		if last_unlocked_position > screenW then
			levels_scroll_widget:scrollToPosition({x = -last_unlocked_position + halfW, time = 500})
		end
	end	
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
	elseif phase == "did" then
		-- remove scroll view
		levels_scroll_widget:removeSelf()
	end	
end

function scene:destroy( event )
	local sceneGroup = self.view
	
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene